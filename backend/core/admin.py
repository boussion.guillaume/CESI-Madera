from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from core.models.component import Component, ComponentFamily
from core.models.customer import Customer
from core.models.model import Model
from core.models.model_component import ModelComponent
from core.models.module import Module
from core.models.module_component import ModuleComponent
from core.models.provider import Provider
from core.models.quote import Quote
from core.models.range import Range
from core.models.specification import Specification
from core.models.unit_of_use import UnitOfUse
from core.models.user import User

admin.site.register(Component)
admin.site.register(ComponentFamily)
admin.site.register(Quote)
admin.site.register(Customer)
admin.site.register(Range)
admin.site.register(Provider)
admin.site.register(Specification)
admin.site.register(UnitOfUse)
admin.site.register(Model)
admin.site.register(Module)
admin.site.register(ModelComponent)
admin.site.register(ModuleComponent)
admin.site.register(User, UserAdmin)
