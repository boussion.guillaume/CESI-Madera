from django.db import models


class Provider(models.Model):
    name = models.CharField(null=False, max_length=255)
    contact = models.CharField(null=False, max_length=255)
    email = models.EmailField(null=False)
    website = models.URLField(null=False)
    phone = models.CharField(null=False, max_length=15)
    fax = models.CharField(null=True, max_length=15)
    address = models.CharField(null=False, max_length=255)

    class Meta:
        verbose_name = "provider"
        verbose_name_plural = "providers"

    def __str__(self):
        return self.name
