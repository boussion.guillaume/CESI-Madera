from django.db import models


class Specification(models.Model):
    choices = (
        ('sec_m', 'Section en m'),
        ('sec_cm', 'Section en cm'),
        ('sec_mm', 'Section en mm'),
        ('ep_cm', 'Epaisseur en cm'),
        ('ep_mm', 'Epaisseur en mm'),
        ('long_larg_m', 'Longueur et largeur en m'),
        ('long_larg_cm', 'Longueur et largeur en cm'),
        ('long_larg_mm', 'Longueur et largeur en mm'),
        ('long_larg_haut_m', 'Longueur, largeur et hauteur en m'),
        ('long_larg_haut_cm', 'Longueur, largeur et hauteur en cm'),
        ('long_larg_haut_mm', 'Longueur, largeur et hauteur en mm'),
    )
    unit = models.CharField(null=False, max_length=255, choices=choices)
    name = models.CharField(null=False, max_length=255)

    class Meta:
        verbose_name = "specification"
        verbose_name_plural = "specifications"

    def __str__(self):
        return self.name
