from django.db import models


class Customer(models.Model):
    first_name = models.CharField(null=False, max_length=255)
    last_name = models.CharField(null=False, max_length=255)
    email = models.EmailField(null=False)
    phone = models.CharField(null=False, max_length=15)
    address = models.CharField(null=False, max_length=255)

    class Meta:
        verbose_name = "customer"
        verbose_name_plural = "customers"

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)