from django.core.validators import MinValueValidator
from django.db import models

from core.models.component_family import ComponentFamily
from core.models.provider import Provider
from core.models.specification import Specification
from core.models.unit_of_use import UnitOfUse


class Component(models.Model):
    name = models.CharField(null=False, max_length=255)
    amount = models.DecimalField(default=0, max_digits=8, decimal_places=2, validators=[MinValueValidator(0)])
    stock = models.DecimalField(default=0, max_digits=8, decimal_places=2, validators=[MinValueValidator(0)])
    component_family = models.ForeignKey(ComponentFamily, on_delete=models.CASCADE)
    specification = models.ForeignKey(Specification, on_delete=models.DO_NOTHING)
    unit_of_use = models.ForeignKey(UnitOfUse, on_delete=models.DO_NOTHING)
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "component"
        verbose_name_plural = "components"

    def __str__(self):
        return self.name
