from django.core.validators import MinValueValidator
from django.db import models

from core.models import Component
from core.models.module import Module


class ModuleComponent(models.Model):
    proportion = models.DecimalField(default=0, max_digits=8, decimal_places=2, validators=[MinValueValidator(0)])
    module = models.ForeignKey(Module, on_delete=models.CASCADE)
    component = models.ForeignKey(Component, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "module component"
        verbose_name_plural = "modules components"

    def __str__(self):
        return '%s - %s' % (self.module.name, self.component.name)