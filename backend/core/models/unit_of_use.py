from django.db import models


class UnitOfUse(models.Model):
    choices = (
        ('m', 'Longueur en mètre'),
        ('piece', 'Pièce'),
        ('m2', 'Surface en m²'),
    )
    unit = models.CharField(null=False, max_length=255, choices=choices)
    name = models.CharField(null=False, max_length=255)

    class Meta:
        verbose_name = "unit of use"
        verbose_name_plural = "units of use"

    def __str__(self):
        return self.unit