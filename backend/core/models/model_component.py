from django.core.validators import MinValueValidator
from django.db import models

from core.models import Model, Component


class ModelComponent(models.Model):
    proportion = models.DecimalField(default=0, max_digits=8, decimal_places=2, validators=[MinValueValidator(0)])
    model = models.ForeignKey(Model, on_delete=models.CASCADE)
    component = models.ForeignKey(Component, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "model component"
        verbose_name_plural = "models components"

    def __str__(self):
        return '%s - %s' % (self.model.name, self.component.name)
