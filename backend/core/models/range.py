from django.db import models


class Range(models.Model):
    name = models.CharField(null=False, max_length=255)

    class Meta:
        verbose_name = "range"
        verbose_name_plural = "ranges"

    def __str__(self):
        return self.name