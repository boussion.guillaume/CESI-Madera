from django.db import models


class ComponentFamily(models.Model):
    name = models.CharField(null=False, max_length=255)

    class Meta:
        verbose_name = "component family"
        verbose_name_plural = "components families"

    def __str__(self):
        return self.name
