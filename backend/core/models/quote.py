from django.contrib.auth import get_user_model
from django.core.validators import MinValueValidator
from django.db import models
from django.utils import timezone

from core.models.customer import Customer
from django.conf import settings


class Quote(models.Model):
    name = models.CharField(null=False, max_length=255)
    amount = models.DecimalField(default=0, max_digits=8, decimal_places=2, validators=[MinValueValidator(0)])
    date = models.DateTimeField(auto_now_add=timezone.now())
    file = models.CharField(null=True, max_length=255)
    STATUS_CHOICES = (
        ('in_progress', 'En cours'),
        ('validated', 'Validé'),
        ('refused', 'Refusé')
    )
    status = models.CharField(null=True, choices=STATUS_CHOICES, max_length=255)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = "quote"
        verbose_name_plural = "quotes"

    def __str__(self):
        return '#%s %s - %s %s' % (self.id, self.name, self.customer.first_name, self.customer.last_name)
