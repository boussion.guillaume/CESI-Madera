from django.db import models

from core.models.range import Range
from core.models.quote import Quote
from core.models.specification import Specification
from core.models.unit_of_use import UnitOfUse


class Module(models.Model):
    name = models.CharField(null=False, max_length=255)
    specification = models.ForeignKey(Specification, on_delete=models.CASCADE)
    unit_of_use = models.ForeignKey(UnitOfUse, on_delete=models.CASCADE)
    quote = models.ForeignKey(Quote, on_delete=models.CASCADE)
    range = models.ForeignKey(Range, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Module"
        verbose_name_plural = "Modules"

    def __str__(self):
        return self.name
