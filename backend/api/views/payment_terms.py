import os
from django.conf import settings
from rest_framework.parsers import JSONParser

from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
import json


class PaymentTermsView(APIView):
    PAYMENT_TERMS_JSON_DIR_FILE = os.path.join(settings.BASE_DIR, './data/payment-terms.json')
    # COMMISSION_JSON_DIR_FILE = 'E:/CESI/MADERA/backend/data/commissions.json'
    JSON_PROPERTY_LIST = ["signatureTerm", "constructionAgreement", "constructionOpening", "fondationsCompletion",
                          "wallsCompletion", "roofCompletion", "equipmentCompletion", "keysHandover"]
    permissions = [IsAuthenticated]
    parser_classes = [JSONParser]
    json_data = {}

    def __init__(self, **kwargs):
        super(PaymentTermsView, self).__init__(**kwargs)
        # check si le fichier existe
        if os.path.isfile(self.PAYMENT_TERMS_JSON_DIR_FILE):
            with open(self.PAYMENT_TERMS_JSON_DIR_FILE) as j:
                self.json_data = json.load(j)
        else:
            self.create_json_if_not_exists()

    def is_json_valid(self, payload):

        for prop in self.JSON_PROPERTY_LIST:

            if prop not in payload and \
                    not type(prop) in [float, int]:
                return False

        self.json_data = payload
        return True

    def create_json_if_not_exists(self):
        # Instanciation du json si pas présent
        self.json_data = {
            "signatureTerm": 0,
            "constructionAgreement": 0,
            "constructionOpening": 0,
            "fondationsCompletion": 0,
            "wallsCompletion": 0,
            "roofCompletion": 0,
            "equipmentCompletion": 0,
            "keysHandover": 0

        }
        with open(self.PAYMENT_TERMS_JSON_DIR_FILE, 'w') as output:
            json.dump(self.json_data, output)

    def save_to_json_file(self):
        with open(self.PAYMENT_TERMS_JSON_DIR_FILE, 'w') as i:
            json.dump(self.json_data, i)

    def get(self, request, *args, **kwargs):
        return Response(self.json_data, status=status.HTTP_200_OK)

    def post(self, request):
        if request.user.is_superuser:
            if self.is_json_valid(request.data):
                self.save_to_json_file()
                return Response(request.data, status=status.HTTP_202_ACCEPTED)
            else:
                return Response("Bad parameters", status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)

