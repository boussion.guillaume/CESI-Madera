from rest_framework import status
from rest_framework.filters import OrderingFilter
from rest_framework.generics import GenericAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.mixins import ListModelMixin
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response

from api.serializers.unit_of_use import UnitOfUseSerializer
from core.models.unit_of_use import UnitOfUse


class UnitOfUseView(ListModelMixin, GenericAPIView):
    queryset = UnitOfUse.objects.all()
    serializer_class = UnitOfUseSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    filter_backends = (OrderingFilter,)
    pagination_class = None

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request):
        serializer = UnitOfUseSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SingleUnitOfUseView(RetrieveUpdateDestroyAPIView):
    queryset = UnitOfUse.objects.all()
    serializer_class = UnitOfUseSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
