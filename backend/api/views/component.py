from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.generics import GenericAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.mixins import ListModelMixin
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response

from api.serializers.component import ComponentSerializer, SingleComponentSerializer
from core.models import Component


class ComponentView(ListModelMixin, GenericAPIView):
    queryset = Component.objects.all()
    serializer_class = ComponentSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    filter_backends = (OrderingFilter, DjangoFilterBackend, SearchFilter)
    filter_fields = ['component_family']
    search_fields = ['name']

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request):
        serializer = SingleComponentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SingleComponentView(RetrieveUpdateDestroyAPIView):
    queryset = Component.objects.all()
    serializer_class = SingleComponentSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
