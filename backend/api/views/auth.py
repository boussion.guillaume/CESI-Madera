from django.contrib.auth import logout
from django.shortcuts import get_object_or_404
from rest_framework import views, status, permissions
from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.response import Response

from api.serializers.user import UserStatusSerializer


class LogInView(ObtainAuthToken):
    """
    A view for login user.
    """

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        if serializer.is_valid():
            user = serializer.validated_data['user']
            token, created = Token.objects.get_or_create(user=user)
            return Response({
                'token': token.key,
                'id': user.id,
                'username': user.username,
                'first_name': user.first_name,
                'last_name': user.last_name,
                'email': user.email,
                'is_staff': user.is_staff,
                'is_superuser': user.is_superuser,
                'is_active': user.is_active
            })
        else:
            return Response({'error': 'Identifiant ou mot de passe incorrect'}, status=status.HTTP_401_UNAUTHORIZED)


class UserByTokenView(viewsets.ViewSet):
    """
    A view for retrieving user by token.
    """

    def retrieve(self, request, key=None):
        queryset = Token.objects.all()
        token = get_object_or_404(queryset, key=key)
        serializer = UserStatusSerializer(token.user)
        return Response(serializer.data)


class LogOutView(views.APIView):
    """
    A view for logout user.
    """
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    #
    # def get(self, request, format=None):
    #     request.user.auth_token.delete()
    #     return Response(status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        # print(self.request.user.username)
        request.user.auth_token.delete()
        return Response({'success': 'successfully'}, status=status.HTTP_200_OK)
