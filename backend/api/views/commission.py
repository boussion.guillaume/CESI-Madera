import os
from django.conf import settings
from rest_framework.parsers import JSONParser

from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
import json


class CommissionView(APIView):
    COMMISSION_JSON_DIR_FILE = os.path.join(settings.BASE_DIR, './data/commissions.json')
    # COMMISSION_JSON_DIR_FILE = 'E:/CESI/MADERA/backend/data/commissions.json'
    permissions = [IsAuthenticated]
    parser_classes = [JSONParser]
    json_data = {}

    def __init__(self, **kwargs):
        super(CommissionView, self).__init__(**kwargs)
        # check si le fichier existe
        if os.path.isfile(self.COMMISSION_JSON_DIR_FILE):
            with open(self.COMMISSION_JSON_DIR_FILE) as j:
                self.json_data = json.load(j)
        else:
            self.create_json_if_not_exists()

    def is_json_valid(self, payload):
        if "enterpriseCommission" in payload and "commercialCommission" in payload:
            if type(self.json_data['enterpriseCommission'] in [float, int]) and\
                    type(self.json_data['commercialCommission'] in [float, int]):
                self.json_data = payload
                return True

    def create_json_if_not_exists(self):
        # Instanciation du json si pas présent
        self.json_data = {
            "enterpriseCommission": 0,
            "commercialCommission": 0
        }
        with open(self.COMMISSION_JSON_DIR_FILE, 'w') as output:
            json.dump(self.json_data, output)

    def save_to_json_file(self):
        with open(self.COMMISSION_JSON_DIR_FILE, 'w') as i:
            json.dump(self.json_data, i)

    def get(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return Response(self.json_data, status=status.HTTP_200_OK)
        else:
            return Response('Unauthorized', status=status.HTTP_401_UNAUTHORIZED)

    def post(self, request):
        if request.user.is_superuser:
            if self.is_json_valid(request.data):
                self.save_to_json_file()
                return Response(request.data, status=status.HTTP_202_ACCEPTED)
            else:
                return Response(request.data, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(request.data, status=status.HTTP_401_UNAUTHORIZED)


