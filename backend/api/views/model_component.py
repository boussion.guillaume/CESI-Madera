from rest_framework import status
from rest_framework.filters import OrderingFilter
from rest_framework.generics import GenericAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.mixins import ListModelMixin
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response

from api.serializers.model_component import ModelComponentSerializer
from core.models.model_component import ModelComponent


class ModelComponentView(ListModelMixin, GenericAPIView):
    queryset = ModelComponent.objects.all()
    serializer_class = ModelComponentSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    filter_backends = (OrderingFilter,)
    pagination_class = None

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SingleModelComponentView(RetrieveUpdateDestroyAPIView):
    queryset = ModelComponent.objects.all()
    serializer_class = ModelComponentSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
