import json
import logging
import os

from django.conf import settings
from django.utils import timezone
from rest_framework.views import APIView

from invoice_generator.InvoiceGenerator.api import Invoice, Item, Client, Provider, Creator
from rest_framework import status
from rest_framework.filters import OrderingFilter
from rest_framework.generics import GenericAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.mixins import ListModelMixin
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response

from invoice_generator.InvoiceGenerator.pdf import SimpleInvoice
from api.serializers.quote import QuoteSerializer
from core.models import Quote, Module, ModuleComponent

logger = logging.getLogger('madera')

# choose english as language
os.environ["INVOICE_LANG"] = "fr"


class QuoteView(ListModelMixin, GenericAPIView):
    queryset = Quote.objects.all()
    serializer_class = QuoteSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    filter_backends = (OrderingFilter,)

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request):
        data = request.data
        customer = data['customer']
        project_name = data['projectName']
        modules = data['modules']
        logger.error('modules : %s ' % modules)
        try:
            quote = Quote.objects.create(name=project_name,
                                         amount=data['total_ht'],
                                         date=timezone.now(),
                                         status="in_progress",
                                         customer_id=customer['id'],
                                         user=request.user, )
            client = Client('%s %s' % (quote.customer.first_name, quote.customer.last_name),
                            email=quote.customer.email,
                            phone=quote.customer.phone,
                            address=quote.customer.address)
            provider = Provider('Madera', email='contact@madera.fr', phone='0474589814')
            creator = Creator('%s %s' % (quote.user.first_name, quote.user.last_name))

            invoice = Invoice(client, provider, creator)
            invoice.title = project_name
            invoice.currency = '€'
            invoice.currency_locale = 'fr_FR.UTF-8'
            for md in modules:
                module = Module.objects.create(name=md['name'],
                                               range_id=md['model']['range'],
                                               specification_id=md['model']['specification'],
                                               unit_of_use_id=md['model']['unitOfUse'],
                                               quote=quote)
                for md_cp in md['moduleComponents']:
                    module_component = ModuleComponent.objects.create(proportion=md_cp['component_proportion'],
                                                                      module=module,
                                                                      component_id=md_cp['component_id'])
                    invoice.add_item(Item(count=md_cp['quantity'],
                                          price=module_component.component.amount,
                                          description='%s - %s' % (module.name, module_component.component.name),
                                          unit=module_component.component.unit_of_use.unit,
                                          tax=20))
            # invoice.add_item(Item(60, 50, description="Item 2", tax=21))
            # invoice.add_item(Item(50, 60, description="Item 3", tax=0))
            # invoice.add_item(Item(5, 600, description="Item 4", tax=15))
            pdf = SimpleInvoice(invoice)
            filename = "%s-%s_%s.pdf" % (quote.customer.first_name.lower(),
                                         quote.customer.last_name.lower(),
                                         quote.name.replace(' ', '-'))
            logger.error(filename)
            pdf.gen('%s%s%s' % (settings.BASE_DIR, settings.MEDIA_ROOT, filename), generate_qr_code=False)
            quote.file = settings.MEDIA_URL + filename
            quote.save()
        except Exception as e:
            logger.error(e)
            return Response(status=status.HTTP_400_BAD_REQUEST)

        logger.error('data : %s' % request.data)
        return Response(status=status.HTTP_201_CREATED)
        # serializer = QuoteSerializer(data=request.data)
        # if serializer.is_valid():
        #     serializer.save()
        #     return Response(serializer.data, status=status.HTTP_201_CREATED)
        # return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SingleQuoteView(RetrieveUpdateDestroyAPIView):
    queryset = Quote.objects.all()
    serializer_class = QuoteSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class QuoteStatView(APIView):
    def get(self, request):
        quotes = Quote.objects.all()
        months = {
            "1": 0,
            "2": 0,
            "3": 0,
            "4": 0,
            "5": 0,
            "6": 0,
            "7": 0,
            "8": 0,
            "9": 0,
            "10": 0,
            "11": 0,
            "12": 0,
        }
        for quote in quotes:
            logger.error(months[str(quote.date.month)])
            months[str(quote.date.month)] += 1
            logger.error('month : %s ' % quote.date.month)
        return Response(months, content_type="application/json", status=status.HTTP_200_OK)

