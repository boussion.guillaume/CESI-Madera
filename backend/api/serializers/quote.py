from rest_framework import serializers
from api.serializers.customer import CustomerSerializer
from api.serializers.user import CommercialSerializer

from core.models import Quote


class QuoteSerializer(serializers.ModelSerializer):
    customer = CustomerSerializer(read_only=True)
    user = CommercialSerializer(read_only=True)

    class Meta:
        model = Quote
        fields = ('id', 'name', 'amount', 'date', 'file', 'status', 'customer', 'user')
