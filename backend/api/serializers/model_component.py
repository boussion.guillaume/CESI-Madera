from rest_framework import serializers

from api.serializers.component import ComponentSerializer
from core.models.model_component import ModelComponent


class ModelComponentSerializer(serializers.ModelSerializer):
    component = ComponentSerializer(many=False, read_only=True)

    class Meta:
        model = ModelComponent
        fields = ('id', 'proportion', 'model', 'component')
