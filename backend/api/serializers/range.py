from rest_framework import serializers

from core.models import Range


class RangeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Range
        fields = ('id', 'name')
