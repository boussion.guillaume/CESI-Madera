from rest_framework import serializers

from core.models.module_component import ModuleComponent


class ModuleComponentSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModuleComponent
        fields = ('id', 'proportion', 'module', 'component')
