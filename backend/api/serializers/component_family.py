from rest_framework import serializers

from core.models.component_family import ComponentFamily


class ComponentFamilySerializer(serializers.ModelSerializer):
    class Meta:
        model = ComponentFamily
        fields = ('id', 'name')
