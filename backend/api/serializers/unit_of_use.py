from rest_framework import serializers

from core.models.unit_of_use import UnitOfUse


class UnitOfUseSerializer(serializers.ModelSerializer):
    class Meta:
        model = UnitOfUse
        fields = ('id', 'name', 'unit')
