from django.contrib.auth import get_user_model, authenticate
from rest_framework import serializers, exceptions


# class AuthTokenSerializer(serializers.Serializer):
#     email = serializers.EmailField()
#     password = serializers.CharField(style={'input_type': 'password'})
#
#     def validate(self, data):
#         print(data.get('email'))
#         email = data.get('email')
#         password = data.get('password')
#
#         if email and password:
#             print(email)
#             print(password)
#             user = authenticate(email=email, password=password)
#             print(user)
#             if user:
#                 if not user.is_active:
#                     msg = 'User account is disabled.'
#                     raise exceptions.ValidationError(msg)
#             else:
#                 msg = 'Unable to log in with provided credentials.'
#                 raise exceptions.ValidationError(msg)
#         else:
#             msg = 'Must include "email" and "password".'
#             raise exceptions.ValidationError(msg)
#
#         data['user'] = user
#         return data


class UserSerializer(serializers.ModelSerializer):
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)

    def validate(self, data):
        if data['password1'] != data['password2']:
            raise serializers.ValidationError('Passwords must match.')
        return data

    class Meta:
        model = get_user_model()
        fields = ('id', 'username', 'password1', 'password2', 'email', 'first_name',
                  'last_name', 'is_staff', 'is_superuser', 'is_active')
        read_only_fields = ('id',)


class UserStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('is_staff', 'is_superuser', 'is_active')
        read_only_fields = ('is_staff', 'is_superuser', 'is_active')


class CommercialSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('id', 'first_name', 'last_name')
        read_only_fields = ('id',)
