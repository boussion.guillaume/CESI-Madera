from rest_framework import serializers

from core.models.component import Component


class ComponentSerializer(serializers.ModelSerializer):
    component_family = serializers.StringRelatedField(read_only=True)
    provider = serializers.StringRelatedField(read_only=True)
    unit_of_use = serializers.StringRelatedField(read_only=True)
    # specification = serializers.StringRelatedField()
    # unit_of_use = serializers.StringRelatedField()

    class Meta:
        model = Component
        fields = ('id', 'name', 'amount', 'specification', 'unit_of_use', 'component_family', 'stock', 'provider')


class SingleComponentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Component
        fields = ('id', 'name', 'amount', 'specification', 'unit_of_use', 'component_family', 'stock', 'provider')