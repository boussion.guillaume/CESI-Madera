from rest_framework import serializers

from core.models.provider import Provider


class ProviderSerializer(serializers.ModelSerializer):
    fax = serializers.CharField(required=False)

    class Meta:
        model = Provider
        fields = ('id', 'name', 'contact', 'email', 'website', 'phone', 'fax', 'address')
