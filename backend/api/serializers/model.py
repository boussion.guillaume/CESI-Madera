from rest_framework import serializers

from core.models.model import Model


class ModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Model
        fields = ('id', 'name', 'specification', 'unit_of_use', 'range')
