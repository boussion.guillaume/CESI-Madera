from django.urls import path

from api.views import auth, component, quote, model, specification, module, unit_of_use, component_family, provider, \
    customer, range, model_component, commission, payment_terms

urlpatterns = [
    path('quotes/', quote.QuoteView.as_view()),
    path('quotes-stats/', quote.QuoteStatView.as_view()),
    path('quotes/<int:pk>/', quote.SingleQuoteView.as_view()),
    path('customers/', customer.CustomerView.as_view()),
    path('customers/<int:pk>/', customer.SingleCustomerView.as_view()),
    path('components/', component.ComponentView.as_view()),
    path('components/<int:pk>/', component.SingleComponentView.as_view()),
    path('components-families/', component_family.ComponentFamilyView.as_view()),
    path('commissions/', commission.CommissionView.as_view()),
    path('components-families/<int:pk>/', component_family.SingleComponentFamilyView.as_view()),
    path('models/', model.ModelView.as_view()),
    path('models/<int:pk>/', model.SingleModelView.as_view()),
    path('models/range/', model.ModelCount.as_view()),
    path('model-components/', model_component.ModelComponentView.as_view()),
    path('modules/', module.ModuleView.as_view()),
    path('modules/<int:pk>/', module.SingleModuleView.as_view()),
    path('ranges/', range.RangeView.as_view()),
    path('ranges/<int:pk>/', range.SingleRangeView.as_view()),
    path('specifications/', specification.SpecificationView.as_view()),
    path('specifications/<int:pk>/', specification.SingleSpecificationView.as_view()),
    path('units-of-use/', unit_of_use.UnitOfUseView.as_view()),
    path('units-of-use/<int:pk>/', unit_of_use.SingleUnitOfUseView.as_view()),
    path('providers/', provider.ProviderView.as_view()),
    path('providers/<int:pk>/', provider.SingleProviderView.as_view()),
    path('payment-terms/', payment_terms.PaymentTermsView.as_view()),
    path('login/', auth.LogInView.as_view()),
    path('user_token/<slug:key>/', auth.UserByTokenView.as_view({'get': 'retrieve'})),
    path('logout/', auth.LogOutView.as_view()),
]
