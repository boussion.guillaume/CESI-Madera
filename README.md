# Projet MADERA

Projet fil rouge réalisé au cours de la formation RIL (Responsable en Ingénierie Logicielle) suivie au CESI Lyon en alternance. Cette réalisation a fait l'objet d'un cahier des charges, budget et planning prévisionnels. 
Le but était de répondre au besoin supposé d'une entreprise, souhaitant réaliser sa propre application de réalisation de devis et de gestion des composants.

# Technologies utilisées :

- Angular
- Bootstrap
- Django
- MySQL
- Docker 


# Fonctionnalités :

- Module d'authentification et de contrôle du rôle de l'utilisateur (n'a donc pas accès à tous les modules)

- API REST donnant accès à des KPIs sur les données de l'application

- Dashboard sur la page d'accueil avec résumé des informations de l'utilisateur

![ALT](/homepage.png)

- Fonctionnalités de lecture, d'ajout, de modification et de suppression des différentes catégories d'éléments de l'application 

![ALT](/component-adding.png)

![ALT](/component-editing.png)

- Module de conception de devis et d'exportation .pdf du devis conçu

![ALT](/quote-conception.png)

![ALT](/quote-component-list.png)



# Réalisation :

La totalité du projet (documentation, cahier des charges etc) a été réalisée par 4 étudiants, tandis que l'équipe développement (dont je faisais partie) était composée de 2 personnes de ce même groupe.
