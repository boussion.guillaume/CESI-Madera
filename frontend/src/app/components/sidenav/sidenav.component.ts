import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {User} from '../../services/beans/user';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  public user: User;
  isUserLoggedIn: boolean;
  isConfCollapsed: boolean;
  isDevisCollapsed: boolean;

  constructor(private as: AuthService) {
    this.isConfCollapsed = false;
    this.isDevisCollapsed = false;
  }

  ngOnInit(): void {
    this.user = User.getUser();
    this.as.hasPrivileges().subscribe((value) => {
      this.isUserLoggedIn = value;
    });
  }
}
