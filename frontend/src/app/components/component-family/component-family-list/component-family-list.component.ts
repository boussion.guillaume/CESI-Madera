import {Component, OnInit} from '@angular/core';
import {ComponentFamilyService} from '../../../services/component-family.service';
import {Page} from '../../../services/beans/page';
import {ComponentFamily} from '../../../services/beans/component_family';
import {User} from '../../../services/beans/user';
import {Query} from '../../../services/beans/query';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {ComponentFamilyModalComponent} from '../component-family-modal/component-family-modal.component';

@Component({
  selector: 'app-component-family-list',
  templateUrl: './component-family-list.component.html',
  styleUrls: ['./component-family-list.component.css']
})
export class ComponentFamilyListComponent implements OnInit {
  private user: User;
  public query: Query;
  public componentFamilyPage: Page<ComponentFamily>;
  private modalOption: NgbModalOptions = {};

  constructor(private modalService: NgbModal,
              private cfs: ComponentFamilyService) {
    this.query = new Query();
    this.query.ordering = '-id';
    this.query.page = 1;
    this.query.page_size = 25;
  }

  ngOnInit(): void {
    this.user = User.getUser();
    this.getComponentsFamilies();
  }

  getComponentsFamilies(): void {
    this.cfs.getComponentsFamilies(this.query, this.user.token).subscribe(componentFamilyPage => {
      if (!componentFamilyPage) {
        return;
      }
      this.componentFamilyPage = componentFamilyPage;
    });
  }

  onSearchType(event): void {
    if (event.target.value.length > 2 || event.target.value === '') {
      setTimeout(() => {
        this.query.search = event.target.value;
        console.log(this.query.search);
        this.getComponentsFamilies();
      }, 500);
    }
  }

  componentFamilyModal(): void {
    this.modalOption.keyboard = false;
    this.modalOption.size = 'md';
    this.modalService.open(ComponentFamilyModalComponent, this.modalOption);
  }

}
