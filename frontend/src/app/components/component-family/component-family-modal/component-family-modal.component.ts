import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-component-family-modal',
  templateUrl: './component-family-modal.component.html',
  styleUrls: ['./component-family-modal.component.css']
})
export class ComponentFamilyModalComponent implements OnInit {

  constructor(public modal: NgbActiveModal) {
  }

  ngOnInit(): void {
  }

  submit(form: NgForm): void {

  }

}
