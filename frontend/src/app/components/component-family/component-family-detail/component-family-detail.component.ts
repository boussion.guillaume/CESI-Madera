import {Component, OnInit} from '@angular/core';
import {User} from '../../../services/beans/user';
import {Query} from '../../../services/beans/query';
import {NgForm} from '@angular/forms';
import {ComponentService} from '../../../services/component.service';
import {SpecificationService} from '../../../services/specification.service';
import {UnitOfUseService} from '../../../services/unit-of-use.service';
import {ComponentFamilyService} from '../../../services/component-family.service';
import {ProviderService} from '../../../services/provider.service';
import {ActivatedRoute, Router} from '@angular/router';
import {RoutingState} from '../../../services/routing.state';
import {ComponentObject} from '../../../services/beans/component';
import {Specification} from '../../../services/beans/specification';
import {UnitOfUse} from '../../../services/beans/unit-of-use';
import {ComponentFamily} from '../../../services/beans/component_family';
import {Provider} from '../../../services/beans/provider';
import {faChevronLeft} from '@fortawesome/free-solid-svg-icons';
import {Page} from '../../../services/beans/page';

@Component({
  selector: 'app-component-family-detail',
  templateUrl: './component-family-detail.component.html',
  styleUrls: ['./component-family-detail.component.css']
})
export class ComponentFamilyDetailComponent implements OnInit {
  public componentFamily: ComponentFamily;
  public componentPage: Page<ComponentObject>;
  public specifications: Specification[];
  public unitsOfUse: UnitOfUse[];
  public providers: Provider[];
  private user: User;
  public previousRoute: string;
  public query: Query;
  public icons = {
    faChevronLeft
  };

  constructor(private cfs: ComponentFamilyService,
              private cs: ComponentService,
              private ss: SpecificationService,
              private us: UnitOfUseService,
              private ps: ProviderService,
              private route: ActivatedRoute,
              private router: Router,
              private routingState: RoutingState) {
    this.query = new Query();
    this.query.ordering = '-id';
    this.query.page = 1;
    this.query.page_size = 25;
    this.query.component_family = +this.route.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.getComponentFamily();
    this.getComponents();
    this.previousRoute = this.routingState.getPreviousUrl() !== '/index' ? this.routingState.getPreviousUrl() : '/';
  }

  getComponentFamily(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.user = User.getUser();
    this.cfs.getComponentFamily(id, this.user.token).subscribe((componentFamily) => {
      if (!componentFamily) {
        return;
      }
      this.componentFamily = componentFamily;
    }, err => {
      if (err.status === 404){
        this.router.navigate(['/404']);
      }
    });
  }

  getComponents(): void {
    this.cs.getComponents(this.query, this.user.token).subscribe(componentPage => {
      if (!componentPage) {
        return;
      }
      this.componentPage = componentPage;
    });
  }

  getSpecifications(): void {
    this.ss.getSpecifications().subscribe((specifications) => {
      if (!specifications) {
        return;
      }
      this.specifications = specifications;
    });
  }

  getUnitsOfUse(): void {
    this.us.getUnitsOfUse().subscribe((unitsOfUse) => {
      if (!unitsOfUse) {
        return;
      }
      this.unitsOfUse = unitsOfUse;
    });
  }

  getProviders(): void {
    const query = new Query();
    query.page_size = 5000;
    this.ps.getProviders(query, this.user.token).subscribe((providers) => {
      if (!providers) {
        return;
      }
      this.providers = providers.results;
    });
  }

  submit(componentForm: NgForm): void {

  }
}
