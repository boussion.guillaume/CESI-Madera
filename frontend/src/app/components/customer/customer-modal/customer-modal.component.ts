import { Component, OnInit } from '@angular/core';
import {User} from '../../../services/beans/user';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {SpecificationService} from '../../../services/specification.service';
import {UnitOfUseService} from '../../../services/unit-of-use.service';
import {ComponentFamilyService} from '../../../services/component-family.service';
import {ProviderService} from '../../../services/provider.service';
import {NgForm} from '@angular/forms';
import { Customer } from 'src/app/services/beans/customer';
import { CustomerService } from 'src/app/services/customer.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-customer-modal',
  templateUrl: './customer-modal.component.html',
  styleUrls: ['./customer-modal.component.css']
})
export class CustomerModalComponent implements OnInit {

  private user: User;

  constructor(public modal: NgbActiveModal,
              private cs: CustomerService,
              private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.user = User.getUser();
  }
  submit(form: NgForm): void {
    let customerToAdd = new Customer(
      null,
      form.value['firstname'],
      form.value['lastname'],
      form.value['email'],
      form.value['phone'],
      form.value['address'],
    );

    this.cs.addCustomer(customerToAdd.toJSONFiltered(), this.user.token)
      .subscribe( () => {
        this.toastr.success('Client ajouté');
        this.modal.close();
      }, err => {
        if(err.status >= 400 && err.status <= 500) {
          this.toastr.error('Erreur lors de l\'ajout du client');
        }
      })
  }

}
