import {Component, OnInit} from '@angular/core';
import {User} from '../../../services/beans/user';
import {Query} from '../../../services/beans/query';
import {Page} from '../../../services/beans/page';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {Customer} from '../../../services/beans/customer';
import {CustomerService} from '../../../services/customer.service';
import {CustomerModalComponent} from '../customer-modal/customer-modal.component';
import {faEdit, faTrash} from '@fortawesome/free-solid-svg-icons';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {
  private user: User;
  public query: Query;
  public customerPage: Page<Customer>;
  private modalOption: NgbModalOptions = {};
  public icons = {
    faTrashIcon: faTrash,
    faEditIcon: faEdit,
  };

  constructor(private modalService: NgbModal,
              private cs: CustomerService,
              private toastr: ToastrService) {
    this.query = new Query();
    this.query.ordering = '-id';
    this.query.page = 1;
    this.query.page_size = 25;
  }

  ngOnInit(): void {
    this.user = User.getUser();
    this.getCustomers();
  }

  getCustomers(): void {
    this.cs.getCustomers(this.query, this.user.token).subscribe(customerPage => {
      if (!customerPage) {
        return;
      }
      this.customerPage = customerPage;
    });
  }

  onSearchType(event): void {
    if (event.target.value.length > 2 || event.target.value === '') {
      setTimeout(() => {
        this.query.search = event.target.value;
        console.log(this.query.search);
        this.getCustomers();
      }, 500);
    }
  }

  deleteCustomer(id: number): void {
    this.cs.deleteCustomer(id, this.user.token)
      .subscribe(() => {
        this.toastr.success('Client supprimé');
        this.getCustomers();
      }, err => {
        this.toastr.error('Erreur lors de la suppression du client');
      });
  }

  customerModal(): void {
    this.modalOption.keyboard = false;
    // this.modalOption.size = 'xl';
    const modalRef= this.modalService.open(CustomerModalComponent, this.modalOption);
    modalRef.result
      .then(() => {
        this.getCustomers();
      });
  }
}
