import {Component, OnInit} from '@angular/core';
import {User} from '../../../services/beans/user';
import {ComponentService} from '../../../services/component.service';
import {ActivatedRoute, Router} from '@angular/router';
import {RoutingState} from '../../../services/routing.state';
import {CustomerService} from '../../../services/customer.service';
import {ComponentObject} from '../../../services/beans/component';
import {Specification} from '../../../services/beans/specification';
import {UnitOfUse} from '../../../services/beans/unit-of-use';
import {ComponentFamily} from '../../../services/beans/component_family';
import {Provider} from '../../../services/beans/provider';
import {faChevronLeft} from '@fortawesome/free-solid-svg-icons';
import {Customer} from '../../../services/beans/customer';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.css']
})
export class CustomerDetailComponent implements OnInit {
  public customer: Customer;
  private user: User;
  public previousRoute: string;
  public icons = {
    faChevronLeft
  };
  private id: number;

  constructor(private cs: CustomerService,
              private route: ActivatedRoute,
              private router: Router,
              private routingState: RoutingState,
              private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.getCustomer();
    this.previousRoute = this.routingState.getPreviousUrl() !== '/index' ? this.routingState.getPreviousUrl() : '/';
  }

  getCustomer(): void {
    this.user = User.getUser();
    this.cs.getCustomer(this.id, this.user.token).subscribe((customer) => {
      if (!customer) {
        return;
      }
      this.customer = customer;
    });
  }

  update(customerForm: NgForm): void{
    const customerToUpdate = new Customer(
      this.id,
      customerForm.form.value['first_name'],
      customerForm.form.value['last_name'],
      customerForm.form.value['email'],
      customerForm.form.value['phone'],
      customerForm.form.value['address']
    )
    
    this.cs.updateCustomer(customerToUpdate.toJSONFiltered(), this.user.token)
      .subscribe(() => {
        this.toastr.success('Client mis à jour');
        this.getCustomer();
      }, (err) => {
        if (err.status >= 400 && err.status <= 500) {
          this.toastr.error('Erreur lors de la mise à jour du client');
        }
      });
  }

}
