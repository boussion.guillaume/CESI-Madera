import { Component, OnInit } from '@angular/core';
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { Model } from 'src/app/services/beans/model';
import { Page } from 'src/app/services/beans/page';
import { Query } from 'src/app/services/beans/query';
import { Range } from 'src/app/services/beans/range';
import { User } from 'src/app/services/beans/user';
import { ModelService } from 'src/app/services/model.service';
import { RangeService } from 'src/app/services/range.service';
import { RangeDeleteConfirmationComponent } from '../range-delete-confirmation/range-delete-confirmation.component';
import { RangeModalComponent } from '../range-modal/range-modal.component';
import {ToastrService} from 'ngx-toastr'

@Component({
  selector: 'app-range-list',
  templateUrl: './range-list.component.html',
  styleUrls: ['./range-list.component.css']
})
export class RangeListComponent implements OnInit {

  private user: User;
  public query: Query;
  public rangePage: Page<Range>;
  public modelsRelatedTo: Model[];
  public ranges: Range[];
  private modalOption: NgbModalOptions = {};
  public icons = {
    faTrashIcon: faTrash,
    faEditIcon: faEdit,
  }

  constructor(private modalService: NgbModal, 
              private rs: RangeService,
              private ms: ModelService,
              private toastr: ToastrService) {
    this.query = new Query();
    this.query.ordering = '-id';
    this.query.page = 1;
    this.query.page_size = 25;
    this.query.range = null; 
  }

  ngOnInit(): void {
    this.user = User.getUser();
    this.getRanges();
  }

  getRanges(): void{
    this.rs.getRanges(this.user.token)
    .subscribe(rangesPage => {
      if (!rangesPage) {
        return;
      }
      this.rangePage = rangesPage;
    })
  }
  
  openDeleteConfirmationModal(id: number): void {
      // Récupération du nombre de modèle(s) affilié(s) pour afficher une modale
      // si nombre > 0
      let nthModelsRelated : number;
      this.ms.getModelsRelatedToRange(id, this.user.token)
        .subscribe((response) => {
          nthModelsRelated = +response['count'];        

          if (nthModelsRelated >= 1) {
            this.modalOption.keyboard = true;
            this.modalOption.size = 'md';
            const modalRef = this.modalService.open(RangeDeleteConfirmationComponent, this.modalOption);
            modalRef.componentInstance.modelsRelated = nthModelsRelated;
            
            // Si l'utilisateur confirme la suppression dans la modale on supprime la gamme
            modalRef.result
              .then((confirmation) => {
                if(confirmation){
                  this.deleteRange(id);
                }
              })
          } else {
            this.deleteRange(id);
          }
      });
  }

  deleteRange(id : number) : void {

    this.rs.deleteRange(id, this.user.token)
      .subscribe( () => {
        this.toastr.success('Gamme supprimée');
        this.getRanges();
      }, err => {
        this.toastr.error('Erreur lors de la suppression');
      })
  }

  openAddingModal(): void {
      this.modalOption.keyboard = true;
      this.modalOption.size = 'md';
      const modalRef = this.modalService.open(RangeModalComponent, this.modalOption);
      modalRef.result
        .then( () => {
          this.getRanges();
        })
  }

}
