import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RangeDetailComponent } from './range-detail.component';

describe('RangeDetailComponent', () => {
  let component: RangeDetailComponent;
  let fixture: ComponentFixture<RangeDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RangeDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RangeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
