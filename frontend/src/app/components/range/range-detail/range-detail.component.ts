import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/services/beans/user';
import { Range } from 'src/app/services/beans/range';
import { RangeService } from 'src/app/services/range.service';
import { RoutingState } from 'src/app/services/routing.state';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-range-detail',
  templateUrl: './range-detail.component.html',
  styleUrls: ['./range-detail.component.css']
})
export class RangeDetailComponent implements OnInit {
  private user: User;
  public previousRoute: string;
  public range: Range;
  public icons = {
    faChevronLeft
  };

  constructor(private rs: RangeService,
    private route: ActivatedRoute,
    private routingState: RoutingState,
    private router: Router,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.user = User.getUser();
    this.getRange();
    this.previousRoute = this.routingState.getPreviousUrl() !== '/index' ? this.routingState.getPreviousUrl() : '/';   
  }

  getRange(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.user = User.getUser();

    this.rs.getRange(id, this.user.token)
      .subscribe((range) => {
        if (!range) {
          return;
        }
        this.range = range;
      }, err => {
        if (err.status == 404){
          this.router.navigate(['/404'])
        }
      })
  }

  update(rangeForm: NgForm): void{
    let rangeToUpdate = {
      id: this.range.id,
      name: rangeForm.form.value['name']
    } as Range;

    this.rs.updateRange(rangeToUpdate, this.user.token)
      .subscribe(() => {       
        this.toastr.success('Gamme mise à jour');
        this.getRange();
      }, (err) => {
        if (err.status >= 400 && err.status <= 500) {
          this.toastr.error('Erreur lors de la mise à jour');
        }
      });
  }

}
