import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from 'src/app/services/beans/user';
import {Range} from 'src/app/services/beans/range'
import { RangeService } from 'src/app/services/range.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-range-modal',
  templateUrl: './range-modal.component.html',
  styleUrls: ['./range-modal.component.css']
})
export class RangeModalComponent implements OnInit {
  private user: User;

  constructor(private rs: RangeService,
              public modal: NgbActiveModal,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.user = User.getUser();
  }

  submit(rangeForm): void {
    let rangeToAdd = {
      name: rangeForm.value['name']
    } as Range;

    this.rs.addRange(rangeToAdd, this.user.token)
      .subscribe( () => {
        this.toastr.success('Gamme ajoutée');
        this.modal.close();
      }, err => {
        if(err.status >= 400 && err.status <= 500) {
          this.toastr.error('Erreur lors de l\'enregistrement ');
        }
      })

  }


}
