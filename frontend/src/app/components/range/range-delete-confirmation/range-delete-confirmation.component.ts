import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-range-delete-confirmation',
  templateUrl: './range-delete-confirmation.component.html',
  styleUrls: ['./range-delete-confirmation.component.css']
})
export class RangeDeleteConfirmationComponent implements OnInit {
  
  public userConfirmation = false;
  @Input() public modelsRelated;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit(): void { 
  }

  hasToDelete(decision: boolean= false): void{
    this.activeModal.close(decision);
  }

}
