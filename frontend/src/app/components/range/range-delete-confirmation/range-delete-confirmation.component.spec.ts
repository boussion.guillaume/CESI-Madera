import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RangeDeleteConfirmationComponent } from './range-delete-confirmation.component';

describe('RangeDeleteConfirmationComponent', () => {
  let component: RangeDeleteConfirmationComponent;
  let fixture: ComponentFixture<RangeDeleteConfirmationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RangeDeleteConfirmationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RangeDeleteConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
