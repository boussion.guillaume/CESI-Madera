import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {User} from '../../services/beans/user';
import {SharedService} from '../../services/shared.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public initialized = true;
  public loginFailedMessage: string;

  constructor(private authService: AuthService,
              private ss: SharedService,
              private router: Router) {
  }

  ngOnInit(): void {
    if (User.getUser() != null) {
      this.router.navigate(['/']);
    }
  }

  submit(form: NgForm): void {
    this.initialized = false;
    this.authService.logIn(form.value.username, form.value.password).subscribe(user => {
      if (!user) {
        return;
      }
      this.initialized = true;
      this.ss.IsUserLoggedIn.next(true);
      this.router.navigate(['/']);
    }, (message) => {
      for (const [_, value] of Object.entries(message.error)) {
        this.loginFailedMessage = value.toString();
      }
    });
  }

}
