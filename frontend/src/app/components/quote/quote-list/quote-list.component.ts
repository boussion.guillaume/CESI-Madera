import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../services/beans/user';
import {Query} from '../../../services/beans/query';
import {Page} from '../../../services/beans/page';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {Quote} from '../../../services/beans/quote';
import {QuoteService} from '../../../services/quote.service';

@Component({
  selector: 'app-quote-list',
  templateUrl: './quote-list.component.html',
  styleUrls: ['./quote-list.component.css']
})
export class QuoteListComponent implements OnInit {
  @Input() pageSize?: number;
  @Input() readOnly: boolean;
  private user: User;
  public query: Query;
  public quotePage: Page<Quote>;
  private modalOption: NgbModalOptions = {};

  constructor(private modalService: NgbModal,
              private qs: QuoteService) {
    this.query = new Query();
    this.query.ordering = '-id';
    this.query.page = 1;
    if (this.pageSize){
      this.query.page_size = this.pageSize;
    } else {
      this.query.page_size = 25;
    }
  }

  ngOnInit(): void {
    this.user = User.getUser();
    this.getQuotes();
  }

  getQuotes(): void {
    this.qs.getQuotes(this.query, this.user.token).subscribe((quotePage) => {
      if (!quotePage){
        return;
      }
      this.quotePage = quotePage;
    });
  }

  quoteModal(): void {

  }

  onSearchType(event): void {
    if (event.target.value.length > 2 || event.target.value === '') {
      setTimeout(() => {
        this.query.search = event.target.value;
        console.log(this.query.search);
        this.getQuotes();
      }, 500);
    }
  }
}
