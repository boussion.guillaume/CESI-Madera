import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {CustomerService} from '../../../services/customer.service';
import {Query} from '../../../services/beans/query';
import {User} from '../../../services/beans/user';
import {Customer} from '../../../services/beans/customer';
import {RangeService} from '../../../services/range.service';
import {ModelService} from '../../../services/model.service';
import {Model} from '../../../services/beans/model';
import {ModelComponentService} from '../../../services/model-component.service';
import {ModelComponent} from '../../../services/beans/model_component';
import {NgForm} from '@angular/forms';
import {QuoteService} from '../../../services/quote.service';
import {Router} from '@angular/router';

interface IModule {
  name: string;
  model: Model;
  quantity: number;
  moduleComponents: object[];
  total: number;
}

interface IQuoteData {
  customer?: Customer;
  projectName?: string;
  houseRange?: string;
  modules?: IModule[];
  total_ht?: number;
  total_ttc?: number;
}

@Component({
  selector: 'app-quote-form',
  templateUrl: './quote-form.component.html',
  styleUrls: ['./quote-form.component.css']
})
export class QuoteFormComponent implements OnInit {
  private user: User;
  public activedStep = 1;
  public searchedCustomer = 'fullname';
  public searchedModel = 'name';
  public customerList = [];
  public modelList = [];
  public rangeList = [];
  public modelComponentList: ModelComponent[] = [];
  public quoteData: IQuoteData = {};
  public modelToDuplicate: Model;

  constructor(private cs: CustomerService,
              private rs: RangeService,
              private ms: ModelService,
              private qs: QuoteService,
              private router: Router,
              private mcps: ModelComponentService) {
    this.quoteData = {
      customer: null,
      projectName: '',
      houseRange: '',
      modules: [],
      total_ht: 0,
      total_ttc: 0
    };
  }

  ngOnInit(): void {
    this.user = User.getUser();
    this.getCustomers();
    this.getRanges();
  }

  getCustomers(): void {
    const query = new Query();
    query.ordering = '-id';
    query.page_size = 500;
    this.cs.getCustomers(query, this.user.token).subscribe(customerPage => {
      if (!customerPage) {
        return;
      }
      this.customerList = customerPage.results;
    });
  }

  getRanges(): void {
    const query = new Query();
    this.rs.getRanges(this.user.token).subscribe(ranges => {
      if (!ranges) {
        return;
      }
      console.log(ranges.results);
      this.rangeList = ranges.results;
    });
  }

  getModels(range?: number): void {
    const query = new Query();
    query.ordering = '-id';
    query.page_size = 1000;
    if (range) {
      query.range = range;
    }
    this.ms.getModels(query, this.user.token).subscribe(modelPage => {
      if (!modelPage) {
        return;
      }
      this.modelList = modelPage.results;
    });
  }

  getModelComponents(): void {
    const query = new Query();
    query.ordering = 'name';
    query.page_size = 1000;
    this.mcps.getModelComponents(this.user.token).subscribe(modelComponentList => {
      if (!modelComponentList) {
        return;
      }
      this.modelComponentList = modelComponentList;
    });
  }

  selectCustomerEvent(item): void {
    this.quoteData.customer = item;
  }

  onChangeCustomerSearch(val: string): void {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  selectModelEvent(item): void {
    this.modelToDuplicate = item;
  }

  prevStep(step: number): void {
    if (step === 0) {
      return;
    }
    this.activedStep = step - 1;
  }

  nextStep(step: number): void {
    if (step === 5) {
      return;
    }
    if (step === 4 && this.quoteData.modules[0].moduleComponents.length === 0) {
      return;
    }
    this.activedStep = step + 1;
    this.getModelComponents();
  }

  onTabChange($event): void {
    this.activedStep = $event.nextId;
    if (this.activedStep === 4) {
      this.getModelComponents();
    }
    console.log(this.quoteData);
  }

  handleFieldChange(value: string, $event: any): void {
    this.quoteData[$event.target.name] = value;
  }

  handleRangeChange(value: string, $event: any): void {
    this.quoteData[$event.target.name] = value;
    this.getModels(+value);
  }

  addModule(name: any, quantity: any, model: any): void {
    if (!this.quoteData.modules) {
      this.quoteData.modules = [];
    }
    this.quoteData.modules.push({
      name: name.value,
      model: this.modelToDuplicate,
      quantity: +quantity.value,
      moduleComponents: [],
      total: 0
    });
    name.value = '';
    quantity.value = null;
    model.clear();
    model.close();
  }

  canNext(): boolean {
    if (this.activedStep === 1 && (!this.quoteData.customer)) {
      return true;
    } else if (this.activedStep === 2 && (!this.quoteData.customer || !this.quoteData.projectName || !this.quoteData.houseRange)) {
      return true;
    } else if (this.activedStep === 3 && (!this.quoteData.customer || !this.quoteData.projectName || !this.quoteData.houseRange || !this.quoteData.modules)) {
      return true;
    } else {
      return false;
    }
  }

  submitComponentModuleForm(form: NgForm): void {
    for (const module of this.quoteData.modules) {
      if (module.moduleComponents.length > 0) {
        module.moduleComponents = [];
      }
      for (const k of Object.keys(form.value)) {
        const splitedKey = k.split('-');
        const modelId = splitedKey[1];
        if (modelId === module.model.id.toString()) {
          if (splitedKey[0] === 'moduleComponentQuantity') {
            const componentId = splitedKey[2];
            const componentName = form.value['moduleComponentName-' + modelId + '-' + componentId];
            const quantity = form.value['moduleComponentQuantity-' + modelId + '-' + componentId];
            const amount = form.value['moduleComponentAmount-' + modelId + '-' + componentId];
            const proportion = form.value['moduleComponentProportion-' + modelId + '-' + componentId];
            const unitOfUse = form.value['moduleComponentUnit-' + modelId + '-' + componentId];
            const moduleComponent = {
              component_id: componentId,
              component_name: componentName,
              component_amount: amount,
              component_proportion: proportion,
              unit_of_use: unitOfUse,
              quantity,
            };
            module.moduleComponents.push(moduleComponent);
            if (module.total === null) {
              module.total = 0;
            }
            module.total += quantity * amount;
            console.log(module.name + ': ' + module.total);
          }
        }
      }
      this.quoteData.total_ht += module.total;
    }
    console.log('quote:');
    console.log(this.quoteData);
    this.quoteData.total_ttc = this.quoteData.total_ht * 1.21;
    this.nextStep(this.activedStep);
  }

  submitQuote(): void {
    this.qs.createQuote(this.quoteData, this.user.token).subscribe((quote) => {
      this.router.navigate(['/quotes']);
    });
  }
}
