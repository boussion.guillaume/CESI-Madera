import {Component, OnInit} from '@angular/core';
import {User} from '../../../services/beans/user';
import {Query} from '../../../services/beans/query';
import {faChevronLeft} from '@fortawesome/free-solid-svg-icons';
import {ActivatedRoute, Router} from '@angular/router';
import {RoutingState} from '../../../services/routing.state';
import {Quote} from '../../../services/beans/quote';
import {QuoteService} from '../../../services/quote.service';
import {ModuleService} from '../../../services/module.service';
import {ModuleComponentService} from '../../../services/module-component.service';
import {Module} from '../../../services/beans/module';
import {NgForm} from '@angular/forms';
import {PaymentTermsSerivce} from '../../../services/payment-terms.service';
import {PaymentTerms} from '../../../services/beans/payment-terms';

@Component({
  selector: 'app-quote-detail',
  templateUrl: './quote-detail.component.html',
  styleUrls: ['./quote-detail.component.css']
})
export class QuoteDetailComponent implements OnInit {
  public quote: Quote;
  public modules: Module[];
  public paymentTerms: PaymentTerms;
  private user: User;
  public previousRoute: string;
  public query: Query;
  public icons = {
    faChevronLeft
  };
  public file: string;

  constructor(private qs: QuoteService,
              private ms: ModuleService,
              private mcps: ModuleComponentService,
              private pt: PaymentTermsSerivce,
              private route: ActivatedRoute,
              private router: Router,
              private routingState: RoutingState) {
    this.query = new Query();
    this.query.ordering = '-id';
    this.query.page = 1;
    this.query.page_size = 25;
    this.query.component_family = +this.route.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.getQuote();
    this.previousRoute = this.routingState.getPreviousUrl() !== '/index' ? this.routingState.getPreviousUrl() : '/';
  }

  getQuote(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.user = User.getUser();
    this.qs.getQuote(id, this.user.token).subscribe((quote) => {
      if (!quote) {
        return;
      }
      this.quote = quote;
      this.file = Quote.get_file_url(quote);
      this.query.quote = +quote.id;
      this.pt.getPaymentTerms(this.user.token).subscribe((terms) => {
        if (!terms) {
          return;
        }
        this.paymentTerms = terms;
      });
    }, err => {
      if (err.status === 404) {
        this.router.navigate(['/404']);
      }
    });
  }

  submit(quoteForm: NgForm): void {
    let status = '';
    if (quoteForm.value.status === 'Validé') {
      status = 'validated';
    } else if (quoteForm.value.status === 'En cours') {
      status = 'in_progress';
    } else {
      status = 'refused';
    }
    const data = {
      id: this.quote.id,
      name: quoteForm.value.name,
      status
    };
    this.qs.updateQuote(data, this.user.token).subscribe((quote) => {
      if (!quote) {
        return;
      }
      this.quote = quote;
    });
  }
}
