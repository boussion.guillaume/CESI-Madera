import {Component, OnInit} from '@angular/core';
import {ProviderService} from '../../../services/provider.service';
import {User} from '../../../services/beans/user';
import {ActivatedRoute, Router} from '@angular/router';
import {RoutingState} from '../../../services/routing.state';
import {faChevronLeft} from '@fortawesome/free-solid-svg-icons';
import {Provider} from '../../../services/beans/provider';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-provider-detail',
  templateUrl: './provider-detail.component.html',
  styleUrls: ['./provider-detail.component.css']
})
export class ProviderDetailComponent implements OnInit {
  public provider: Provider;
  private user: User;
  public previousRoute: string;
  public icons = {
    faChevronLeft
  };

  constructor(private ps: ProviderService,
              private route: ActivatedRoute,
              private toastr: ToastrService,
              private routingState: RoutingState) {
  }

  ngOnInit(): void {
    this.getProvider();
    this.previousRoute = this.routingState.getPreviousUrl() !== '/index' ? this.routingState.getPreviousUrl() : '/';
  }

  getProvider(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.user = User.getUser();
    this.ps.getProvider(id, this.user.token).subscribe((provider) => {
      if (!provider) {
        return;
      }
      this.provider = provider;
    });
  }

  update(providerForm: NgForm): void {
    const providerToUpdate = new Provider(
      this.provider.id,
      providerForm.value.name,
      providerForm.value.contact,
      providerForm.value.email,
      providerForm.value.website,
      providerForm.value.phone,
      providerForm.value.address,
    );

    if (providerForm.value.fax === ''){
      providerToUpdate.fax = null;
    }

    this.ps.updateProvider(providerToUpdate, this.user.token)
    .subscribe(() => {
      this.toastr.success('Fournisseur mis à jour');
      this.getProvider();
    }, (err) => {
      if (err.status >= 400 && err.status <= 500) {
        this.toastr.error('Erreur lors de la mise à jour du fournisseur');
      }
    });
  }

}
