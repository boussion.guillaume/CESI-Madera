import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {User} from '../../../services/beans/user';
import {NgForm} from '@angular/forms';
import { ProviderService } from 'src/app/services/provider.service';
import { Provider } from 'src/app/services/beans/provider';
import { isEmpty } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-provider-modal',
  templateUrl: './provider-modal.component.html',
  styleUrls: ['./provider-modal.component.css']
})
export class ProviderModalComponent implements OnInit {
  public user: User;

  constructor(public modal: NgbActiveModal,
              private ps: ProviderService,
              private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.user = User.getUser();
  }

  submit(providerForm: NgForm): void {
    const providerToAdd = new Provider(
      null,
      providerForm.value['name'],
      providerForm.value['contact'],
      providerForm.value['email'],
      providerForm.value['website'],
      providerForm.value['phone'],
      providerForm.value['address']
    );

    if(providerForm.value['fax']) {
      providerToAdd.fax = providerForm.value['fax'];
    };

    this.ps.addProvider(providerToAdd, this.user.token)
      .subscribe(() => {
        this.toastr.success('Fournisseur ajouté !')
        this.modal.close();
      }, (err) => {
        if (err.status >= 400 && err.status <= 500) {
          this.toastr.error('Erreur lors de l\'ajout du fournisseur');
        }
      });
  }

}
