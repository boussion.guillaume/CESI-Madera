import {Component, OnInit} from '@angular/core';
import {User} from '../../../services/beans/user';
import {Query} from '../../../services/beans/query';
import {Page} from '../../../services/beans/page';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {Provider} from '../../../services/beans/provider';
import {ProviderService} from '../../../services/provider.service';
import {ProviderModalComponent} from '../provider-modal/provider-modal.component';
import {faEdit, faTrash} from '@fortawesome/free-solid-svg-icons';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-providers-list',
  templateUrl: './providers-list.component.html',
  styleUrls: ['./providers-list.component.css']
})
export class ProvidersListComponent implements OnInit {

  private user: User;
  public query: Query;
  public providerPage: Page<Provider>;
  private modalOption: NgbModalOptions = {};
  public icons = {
    faTrashIcon: faTrash,
    faEditIcon: faEdit,
  };

  constructor(private modalService: NgbModal,
              private ps: ProviderService,
              private toastr: ToastrService) {
    this.query = new Query();
    this.query.ordering = '-id';
    this.query.page = 1;
    this.query.page_size = 25;
  }

  ngOnInit(): void {
    this.user = User.getUser();
    this.getProviders();
  }

  getProviders(): void {
    this.ps.getProviders(this.query, this.user.token).subscribe(providerPage => {
      if (!providerPage) {
        return;
      }
      this.providerPage = providerPage;
    });
  }

  onSearchType(event): void {
    if (event.target.value.length > 2 || event.target.value === '') {
      setTimeout(() => {
        this.query.search = event.target.value;
        console.log(this.query.search);
        this.getProviders();
      }, 500);
    }
  }

  providerModal(): void {
    this.modalOption.keyboard = false;
    this.modalOption.size = 'xl';
    this.modalService.open(ProviderModalComponent, this.modalOption);
  }

  deleteProvider(id: string): void {
    this.ps.deleteProvider(+id, this.user.token)
      .subscribe(() => {
        this.toastr.success('Fournisseur supprimé');
        this.getProviders();
      }, err => {
        this.toastr.error('Erreur lors de la suppression du fournisseur');
      });
  }

}
