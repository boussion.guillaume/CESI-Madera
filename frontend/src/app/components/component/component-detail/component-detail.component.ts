import {Component, OnInit} from '@angular/core';
import {ComponentService} from '../../../services/component.service';
import {ComponentObject} from '../../../services/beans/component';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../../services/beans/user';
import {Query} from '../../../services/beans/query';
import {SpecificationService} from '../../../services/specification.service';
import {UnitOfUseService} from '../../../services/unit-of-use.service';
import {ComponentFamilyService} from '../../../services/component-family.service';
import {ProviderService} from '../../../services/provider.service';
import {Specification} from '../../../services/beans/specification';
import {UnitOfUse} from '../../../services/beans/unit-of-use';
import {ComponentFamily} from '../../../services/beans/component_family';
import {Provider} from '../../../services/beans/provider';
import {NgForm} from '@angular/forms';
import {faCoffee, faChevronLeft} from '@fortawesome/free-solid-svg-icons';
import {RoutingState} from '../../../services/routing.state';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-component-detail',
  templateUrl: './component-detail.component.html',
  styleUrls: ['./component-detail.component.css']
})
export class ComponentDetailComponent implements OnInit {
  public component: ComponentObject;
  public specifications: Specification[];
  public unitsOfUse: UnitOfUse[];
  public componentsFamilies: ComponentFamily[];
  public providers: Provider[];
  private user: User;
  public previousRoute: string;
  private id: number;
  public icons = {
    faChevronLeft
  };

  constructor(private cs: ComponentService,
              private ss: SpecificationService,
              private us: UnitOfUseService,
              private cfs: ComponentFamilyService,
              private ps: ProviderService,
              private route: ActivatedRoute,
              private router: Router,
              private routingState: RoutingState,
              private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.getComponent();
    this.getSpecifications();
    this.getUnitsOfUse();
    this.getComponentsFamilies();
    this.getProviders();
    this.previousRoute = this.routingState.getPreviousUrl() !== '/index' ? this.routingState.getPreviousUrl() : '/';
  }

  getComponent(): void {
    
    this.user = User.getUser();
    this.cs.getComponent(this.id, this.user.token).subscribe((component) => {
      if (!component) {
        return;
      }
      this.component = component;
    });
  }

  getSpecifications(): void {
    this.ss.getSpecifications().subscribe((specifications) => {
      if (!specifications) {
        return;
      }
      this.specifications = specifications;
    });
  }

  getUnitsOfUse(): void {
    this.us.getUnitsOfUse().subscribe((unitsOfUse) => {
      if (!unitsOfUse) {
        return;
      }
      this.unitsOfUse = unitsOfUse;
    });
  }

  getComponentsFamilies(): void {
    const query = new Query();
    query.page_size = 5000;
    this.cfs.getComponentsFamilies(query, this.user.token).subscribe((componentsFamilies) => {
      if (!componentsFamilies) {
        return;
      }
      this.componentsFamilies = componentsFamilies.results;
    });
  }

  getProviders(): void {
    const query = new Query();
    query.page_size = 5000;
    this.ps.getProviders(query, this.user.token).subscribe((providers) => {
      if (!providers) {
        return;
      }
      this.providers = providers.results;
    });
  }

  update(componentForm: NgForm): void {
    const componentToUpdate = new ComponentObject(
      this.id.toString(),
      componentForm.form.value['name'],
      componentForm.form.value['amount'],
      componentForm.form.value['stock'],
      componentForm.form.value['component_family'],
      componentForm.form.value['provider'],
      componentForm.form.value['specification'],
      componentForm.form.value['unit_of_use']
    );

    this.cs.updateComponent(componentToUpdate.toJSON(), this.user.token)
      .subscribe(() => {       
        this.toastr.success('Composant mis à jour');
        this.getComponent();
      }, (err) => {
        if (err.status >= 400 && err.status <= 500) {
          this.toastr.error('Erreur lors de la mise à jour');
        }
      });

  }
}
