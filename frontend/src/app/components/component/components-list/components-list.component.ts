import {Component, OnInit} from '@angular/core';
import {ComponentService} from '../../../services/component.service';
import {User} from '../../../services/beans/user';
import {Query} from '../../../services/beans/query';
import {ComponentObject} from '../../../services/beans/component';
import {Page} from '../../../services/beans/page';
import {ComponentModalComponent} from '../component-modal/component-modal.component';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {faPlus} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-components-list',
  templateUrl: './components-list.component.html',
  styleUrls: ['./components-list.component.css']
})
export class ComponentsListComponent implements OnInit {
  private user: User;
  public query: Query;
  public componentPage: Page<ComponentObject>;
  private modalOption: NgbModalOptions = {};
  public icons = {
    faPlus
  }

  constructor(private modalService: NgbModal,
              private cs: ComponentService) {
    this.query = new Query();
    this.query.ordering = '-id';
    this.query.page = 1;
    this.query.page_size = 25;
  }

  ngOnInit(): void {
    this.user = User.getUser();
    this.getComponents();
  }

  getComponents(): void {
    this.cs.getComponents(this.query, this.user.token).subscribe(componentPage => {
      if (!componentPage) {
        return;
      }
      this.componentPage = componentPage;
    });
  }

  onSearchType(event): void {
    if (event.target.value.length > 2 || event.target.value === '') {
      setTimeout(() => {
        this.query.search = event.target.value;
        console.log(this.query.search);
        this.getComponents();
      }, 500);
    }
  }

  componentModal(): void {
    this.modalOption.keyboard = false;
    this.modalOption.size = 'xl';
    this.modalService.open(ComponentModalComponent, this.modalOption);
  }
}
