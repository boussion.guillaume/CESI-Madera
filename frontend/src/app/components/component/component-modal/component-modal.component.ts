import {Component, OnInit} from '@angular/core';
import {Specification} from '../../../services/beans/specification';
import {SpecificationService} from '../../../services/specification.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {NgForm} from '@angular/forms';
import {UnitOfUseService} from '../../../services/unit-of-use.service';
import {UnitOfUse} from '../../../services/beans/unit-of-use';
import {ComponentFamilyService} from '../../../services/component-family.service';
import {ComponentFamily} from '../../../services/beans/component_family';
import {Query} from '../../../services/beans/query';
import {Provider} from '../../../services/beans/provider';
import {ProviderService} from '../../../services/provider.service';
import {User} from '../../../services/beans/user';

@Component({
  selector: 'app-component-modal',
  templateUrl: './component-modal.component.html',
  styleUrls: ['./component-modal.component.css']
})
export class ComponentModalComponent implements OnInit {
  public specifications: Specification[];
  public unitsOfUse: UnitOfUse[];
  public componentsFamilies: ComponentFamily[];
  public providers: Provider[];
  private user: User;

  constructor(public modal: NgbActiveModal,
              private ss: SpecificationService,
              private us: UnitOfUseService,
              private cfs: ComponentFamilyService,
              private ps: ProviderService) {
  }

  ngOnInit(): void {
    this.user = User.getUser();
    this.getSpecifications();
    this.getUnitsOfUse();
    this.getComponentsFamilies();
    this.getProviders();
  }

  getSpecifications(): void {
    this.ss.getSpecifications().subscribe((specifications) => {
      if (!specifications) {
        return;
      }
      this.specifications = specifications;
    });
  }

  getUnitsOfUse(): void {
    this.us.getUnitsOfUse().subscribe((unitsOfUse) => {
      if (!unitsOfUse) {
        return;
      }
      this.unitsOfUse = unitsOfUse;
    });
  }

  getComponentsFamilies(): void {
    const query = new Query();
    query.page_size = 5000;
    this.cfs.getComponentsFamilies(query, this.user.token).subscribe((componentsFamilies) => {
      if (!componentsFamilies) {
        return;
      }
      this.componentsFamilies = componentsFamilies.results;
    });
  }

  getProviders(): void {
    const query = new Query();
    query.page_size = 5000;
    this.ps.getProviders(query, this.user.token).subscribe((providers) => {
      if (!providers) {
        return;
      }
      this.providers = providers.results;
    });
  }

  submit(form: NgForm): void {

  }
}
