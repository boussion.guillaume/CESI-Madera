import { Component, OnInit } from '@angular/core';
import { Commission } from 'src/app/services/beans/commission';
import { User } from 'src/app/services/beans/user';
import {PaymentTermsSerivce} from '../../services/payment-terms.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { PaymentTerms } from 'src/app/services/beans/payment-terms';

@Component({
  selector: 'app-payment-terms',
  templateUrl: './payment-terms.component.html',
  styleUrls: ['./payment-terms.component.css']
})
export class PaymentTermsComponent implements OnInit {
  public user : User;
  public paymentTerms : PaymentTerms;

  constructor(private pts: PaymentTermsSerivce, private toastr: ToastrService) {
    this.paymentTerms = {
      signatureTerm: 0,
      constructionAgreement: 0,
      constructionOpening: 0,
      fondationsCompletion: 0,
      wallsCompletion: 0,
      roofCompletion: 0,
      equipmentCompletion: 0,
      keysHandover: 0
  }
   }

  ngOnInit(): void {
    this.user = User.getUser();
    this.getPaymentTerms();    
  }

  getPaymentTerms(){
    this.pts.getPaymentTerms(this.user.token)
      .subscribe((paymentTerms) => {
        this.paymentTerms = paymentTerms;        
      });
  }

  update(commissionForm: NgForm): void{
    const paymentTerms = new PaymentTerms(
      +commissionForm.form.value['signatureTerm'],
      +commissionForm.form.value['constructionAgreement'],
      +commissionForm.form.value['constructionOpening'],
      +commissionForm.form.value['fondationsCompletion'],
      +commissionForm.form.value['wallsCompletion'],
      +commissionForm.form.value['roofCompletion'],
      +commissionForm.form.value['equipmentCompletion'],
      +commissionForm.form.value['keysHandover'],
    )
    
    this.pts.updatePaymentTerms(paymentTerms, this.user.token)
      .subscribe(() => {
        this.toastr.success('Modalité de paiement mises à jour');
        this.getPaymentTerms();
      }, (err) => {
        if (err.status >= 400 && err.status <= 500) {
          this.toastr.error('Erreur lors de la mise à jour des modalités de paiement');
        }
      });
  }

}

