import {Component, OnInit} from '@angular/core';
import {User} from '../../services/beans/user';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {Label} from 'ng2-charts';
import {QuoteService} from '../../services/quote.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public user: User;
  barChartOptions: ChartOptions = {
    responsive: true,
  };
  barChartLabels: Label[] = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];

  barChartData: ChartDataSets[] = [
    { data: [], label: 'Nombre de devis' }
  ];

  constructor(private qs: QuoteService) {
  }

  ngOnInit(): void {
    this.user = User.getUser();
    this.qs.getQuoteStats(this.user.token).subscribe((stats) => {

      for (const [key, value] of Object.entries(stats)) {
        console.log("for key " + key + " value " + value);
        this.barChartData[0].data.push(+value);
      }
    });
  }

}
