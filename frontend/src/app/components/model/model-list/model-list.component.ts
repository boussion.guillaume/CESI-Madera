import {Component, OnInit} from '@angular/core';
import {Query} from 'src/app/services/beans/query';
import {User} from 'src/app/services/beans/user';
import {Page} from 'src/app/services/beans/page';
import {ModelService} from 'src/app/services/model.service';
import {Model} from 'src/app/services/beans/model';
import {RangeService} from 'src/app/services/range.service';
import {Range} from 'src/app/services/beans/range';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {ModelModalComponent} from '../model-modal/model-modal.component';
import {faEdit, faTrash} from '@fortawesome/free-solid-svg-icons';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-model-list',
  templateUrl: './model-list.component.html',
  styleUrls: ['./model-list.component.css']
})

export class ModelListComponent implements OnInit {
  private user: User;
  public query: Query;
  public modelPage: Page<Model>;
  public ranges: Range[];
  private modalOption: NgbModalOptions = {};
  public icons = {
    faTrashIcon: faTrash,
    faEditIcon: faEdit,
  };

  constructor(private modalService: NgbModal,
              private ms: ModelService,
              private rs: RangeService,
              private toastr: ToastrService) {
    this.query = new Query();
    this.query.ordering = '-id';
    this.query.page = 1;
    this.query.page_size = 25;
    this.query.range = null;
  }

  ngOnInit(): void {
    this.user = User.getUser();
    this.getModels();
    this.getRanges();
  }

  getModels(): void {
    this.ms.getModels(this.query, this.user.token)
      .subscribe(modelPage => {
        if (!modelPage) {
          return;
        }
        this.modelPage = modelPage;
      });
  }

  getRanges(): void {
    this.rs.getRanges(this.user.token)
      .subscribe(ranges => {
        if (!ranges) {
          return;
        }
        this.ranges = ranges.results;
      });
  }

  onSearchType(event): void {
    if (event.target.value.length > 2 || event.target.value === '') {
      setTimeout(() => {
        this.query.search = event.target.value;
        this.getModels();
      }, 500);
    }
  }

  deleteModel(id: number): void {
    this.ms.deleteModel(id, this.user.token)
      .subscribe(() => {
        this.toastr.success('Modèle supprimé');
        this.getModels();
      }, err => {
        this.toastr.error('Erreur lors de la suppression du modèle');
      });
  }

  openAddingModal(): void {
    this.modalOption.keyboard = true;
    this.modalOption.size = 'xl';
    const modalRef = this.modalService.open(ModelModalComponent, this.modalOption);
    modalRef.result
      .then(() => {
        this.getModels();
      });
  }

}
