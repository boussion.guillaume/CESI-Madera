import { Component, OnInit } from '@angular/core';
import { Model } from 'src/app/services/beans/model';
import { Range } from 'src/app/services/beans/range';
import { User } from 'src/app/services/beans/user';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { ModelService } from 'src/app/services/model.service';
import { RangeService } from 'src/app/services/range.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RoutingState } from 'src/app/services/routing.state';
import { SpecificationService } from 'src/app/services/specification.service';
import { Specification } from 'src/app/services/beans/specification';
import { UnitOfUse } from 'src/app/services/beans/unit-of-use';
import { UnitOfUseService } from 'src/app/services/unit-of-use.service';
import { NgForm } from '@angular/forms';
import { identifierModuleUrl } from '@angular/compiler';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-model-detail',
  templateUrl: './model-detail.component.html',
  styleUrls: ['./model-detail.component.css']
})

export class ModelDetailComponent implements OnInit {
  public model: Model;
  private user: User;
  public previousRoute: string;
  public ranges: Range[];
  public specifications: Specification[];
  public unitsOfUse: UnitOfUse[];
  public icons = {
    faChevronLeft
  };

  constructor(private ms: ModelService,
              private rs: RangeService,
              private ss: SpecificationService,
              private us: UnitOfUseService,
              private route: ActivatedRoute,
              private routingState: RoutingState,
              private router: Router,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.user = User.getUser();
    this.getRanges();
    this.getSpecifications();
    this.getUnitsOfUse();
    this.getModel();
    this.previousRoute = this.routingState.getPreviousUrl() !== '/index' ? this.routingState.getPreviousUrl() : '/';   
  }

  getModel(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.user = User.getUser();

    this.ms.getModel(id, this.user.token)
      .subscribe((model) => {
        if (!model) {
          return;
        }
        this.model = model;
      }, err => {
        if (err.status == 404){
          this.router.navigate(['/404'])
        }
      })
  }

  getRanges(): void{
    this.rs.getRanges(this.user.token).subscribe(ranges => {
      if (!ranges) {
        return;
      }
      this.ranges = ranges.results;
    })
  }

  getSpecifications(): void{
    this.ss.getSpecifications(this.user.token)
      .subscribe(specs => {
      if (!specs) {
        return;
      }
      this.specifications = specs;
    })
  }

  getUnitsOfUse(): void{
    this.us.getUnitsOfUse(this.user.token)
      .subscribe(unitsOfUse => {
      if (!unitsOfUse) {
        return;
      }
      this.unitsOfUse = unitsOfUse;
    })
  }

  update(modelForm: NgForm): void{
    let modelToUpdate = {
      id: this.model.id,
      name: modelForm.form.value['name'],
      range: modelForm.form.value['range'],
      specification: modelForm.form.value['specification'],
      unitOfUse: modelForm.form.value['unitOfUse']
    } as Model;

    this.ms.updateModel(modelToUpdate, this.user.token)
      .subscribe(() => {
        this.toastr.success('Modèle mis à jour !');
        this.getModel();
      }, (err) => {
        if (err.status >= 400 && err.status <= 500) {
          this.toastr.error('Erreur lors de la mise à jour du modèle');
        }
      });
  }


}
