import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Specification } from 'src/app/services/beans/specification';
import { UnitOfUse } from 'src/app/services/beans/unit-of-use';
import { Range } from 'src/app/services/beans/range';
import { RangeService } from 'src/app/services/range.service';
import { SpecificationService } from 'src/app/services/specification.service';
import { UnitOfUseService } from 'src/app/services/unit-of-use.service';
import { User } from 'src/app/services/beans/user';
import { Model } from 'src/app/services/beans/model';
import { ModelService } from 'src/app/services/model.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-model-modal',
  templateUrl: './model-modal.component.html',
  styleUrls: ['./model-modal.component.css']
})
export class ModelModalComponent implements OnInit {
  public ranges: Range[];
  public specifications: Specification[];
  public unitsOfUse: UnitOfUse[];
  private user: User;
  
  constructor(private rs: RangeService,
              private ss: SpecificationService,
              private us: UnitOfUseService,
              private ms: ModelService,
              public modal: NgbActiveModal,
              private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.user = User.getUser();
    this.getRanges();
    this.getSpecifications();
    this.getUnitsOfUse();
  }

  submit(modelForm: NgForm): void {
    let modelToAdd = {
      name: modelForm.value['name'],
      range: modelForm.form.value['range'],
      specification: modelForm.form.value['specification'],
      unitOfUse: modelForm.form.value['unitOfUse']
    } as Model;

    this.ms.addModel(modelToAdd, this.user.token)
      .subscribe(() => {
        this.toastr.success('Modèle ajouté !');
        this.modal.close();
      }, (err) => {
        if (err.status >= 400 && err.status <= 500) {
          this.toastr.error('Erreur lors de l\'ajout du modèle');
        }
      });
  }

  
  getRanges(): void{
    this.rs.getRanges(this.user.token).subscribe(ranges => {
      if (!ranges) {
        return;
      }
      this.ranges = ranges.results;
    })
  }

  getSpecifications(): void{
    this.ss.getSpecifications(this.user.token)
      .subscribe(specs => {
      if (!specs) {
        return;
      }
      this.specifications = specs;
    })
  }

  getUnitsOfUse(): void{
    this.us.getUnitsOfUse(this.user.token)
      .subscribe(unitsOfUse => {
      if (!unitsOfUse) {
        return;
      }
      this.unitsOfUse = unitsOfUse;
    })
  }


}
