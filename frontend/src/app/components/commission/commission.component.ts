import { Component, OnInit } from '@angular/core';
import { Commission } from 'src/app/services/beans/commission';
import { User } from 'src/app/services/beans/user';
import {CommissionService} from '../../services/commission.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-commission',
  templateUrl: './commission.component.html',
  styleUrls: ['./commission.component.css']
})
export class CommissionComponent implements OnInit {
  public user : User;
  public commission : Commission;

  constructor(private cs: CommissionService, private toastr: ToastrService) {
    this.commission = {
      enterprise: 0,
      commercial: 0
    }
   }

  ngOnInit(): void {
    this.user = User.getUser();
    this.getCommissions();    
  }

  getCommissions(){
    this.cs.getCommissions(this.user.token)
      .subscribe((commissions) => {
        this.commission = Commission.parse(commissions);        
      });
  }

  update(commissionForm: NgForm): void{
    const commissions = new Commission(
      +commissionForm.form.value['enterprise'],
      +commissionForm.form.value['commercial']
    )
    
    this.cs.updateCommissions(Commission.toJSON(commissions), this.user.token)
      .subscribe(() => {
        this.toastr.success('Commissions mises à jour');
        this.getCommissions();
      }, (err) => {
        if (err.status >= 400 && err.status <= 500) {
          this.toastr.error('Erreur lors de la mise à jour des commissions');
        }
      });
  }

}
