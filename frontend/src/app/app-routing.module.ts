import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {ComponentsListComponent} from './components/component/components-list/components-list.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {AuthGuard} from './services/auth.guard';
import {ComponentDetailComponent} from './components/component/component-detail/component-detail.component';
import {ComponentFamilyListComponent} from './components/component-family/component-family-list/component-family-list.component';
import {ComponentFamilyDetailComponent} from './components/component-family/component-family-detail/component-family-detail.component';
import {ProvidersListComponent} from './components/provider/providers-list/providers-list.component';
import {ProviderDetailComponent} from './components/provider/provider-detail/provider-detail.component';
import {CustomerListComponent} from './components/customer/customer-list/customer-list.component';
import {CustomerDetailComponent} from './components/customer/customer-detail/customer-detail.component';
import {QuoteFormComponent} from './components/quote/quote-form/quote-form.component';
import {ModelListComponent} from './components/model/model-list/model-list.component';
import {ModelDetailComponent} from './components/model/model-detail/model-detail.component';
import {RangeListComponent} from './components/range/range-list/range-list.component';
import {RangeDetailComponent} from './components/range/range-detail/range-detail.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {QuoteDetailComponent} from './components/quote/quote-detail/quote-detail.component';
import {QuoteListComponent} from './components/quote/quote-list/quote-list.component';
import { PaymentTermsComponent } from './components/payment-terms/payment-terms.component';
import { CommissionComponent } from './components/commission/commission.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: '', component: DashboardComponent, canActivate: [AuthGuard]},
  {path: 'components', component: ComponentsListComponent, canActivate: [AuthGuard]},
  {path: 'components/:id', component: ComponentDetailComponent, canActivate: [AuthGuard]},
  {path: 'components-families', component: ComponentFamilyListComponent, canActivate: [AuthGuard]},
  {path: 'components-families/:id', component: ComponentFamilyDetailComponent, canActivate: [AuthGuard]},
  {path: 'customers', component: CustomerListComponent, canActivate: [AuthGuard]},
  {path: 'customers/:id', component: CustomerDetailComponent, canActivate: [AuthGuard]},
  {path: 'providers', component: ProvidersListComponent, canActivate: [AuthGuard]},
  {path: 'providers/:id', component: ProviderDetailComponent, canActivate: [AuthGuard]},
  {path: 'new-quotes', component: QuoteFormComponent, canActivate: [AuthGuard]},
  {path: 'quotes', component: QuoteListComponent, canActivate: [AuthGuard]},
  {path: 'quotes/:id', component: QuoteDetailComponent, canActivate: [AuthGuard]},
  {path: 'models', component: ModelListComponent, canActivate: [AuthGuard]},
  {path: 'models/:id', component: ModelDetailComponent, canActivate: [AuthGuard]},
  {path: 'ranges', component: RangeListComponent, canActivate: [AuthGuard]},
  {path: 'ranges/:id', component: RangeDetailComponent, canActivate: [AuthGuard]},
  {path: 'commission', component: CommissionComponent, canActivate: [AuthGuard]},
  {path: 'payment-terms', component: PaymentTermsComponent, canActivate: [AuthGuard]},
  {path: '**', redirectTo: '/404', canActivate: [AuthGuard]},
  {path: '404', component: PageNotFoundComponent, canActivate: [AuthGuard]}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
