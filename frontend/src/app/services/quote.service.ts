import {Injectable} from '@angular/core';
import {Model} from './beans/model';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Quote} from './beans/quote';
import {Query} from './beans/query';
import {Page} from './beans/page';
import {ComponentObject} from './beans/component';
import {map} from 'rxjs/operators';
import {ComponentFamily} from './beans/component_family';
import {Range} from './beans/range';

@Injectable({
  providedIn: 'root'
})
export class QuoteService {
  private serverUrl = environment.apiEndpoint;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private http: HttpClient) {
  }

  getQuotes(query: Query, userToken: string): Observable<Page<Quote>> {
    let queryUrl = `${this.serverUrl}api/quotes/?format=json`;
    if (query) {
      for (const [key, value] of Object.entries(query)) {
        if (value) {
          queryUrl += `&${key}=${value}`;
        }
      }
    }
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.get<Page<Quote>>(queryUrl, this.httpOptions)
      .pipe(map(page => {
        page.results = page.results.map(quoteJson => Quote.parse(quoteJson));
        return page;
      }));
  }

  getQuote(id: number, userToken: string): Observable<Quote> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.get<Quote>(`${this.serverUrl}api/quotes/${id}/?format=json`, this.httpOptions)
      .pipe(map(quoteJson => Quote.parse(quoteJson)));
  }

  createQuote(data: any, userToken: string): Observable<Quote> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.post<Quote>(`${this.serverUrl}api/quotes/`, data, this.httpOptions);
  }

  updateQuote(quote: any, userToken: string): Observable<Quote> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.patch<Quote>(`${this.serverUrl}api/quotes/${quote.id}/`, quote, this.httpOptions)
      .pipe(map(quoteJson => Quote.parse(quoteJson)));
  }

  deleteQuote(id: number, userToken: string): Observable<Quote> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.delete<Quote>(`${this.serverUrl}api/quotes/${id}/`, this.httpOptions);
  }

  getQuoteStats(userToken: string): Observable<any> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.get<any>(`${this.serverUrl}api/quotes-stats/?format=json`, this.httpOptions);
  }
}
