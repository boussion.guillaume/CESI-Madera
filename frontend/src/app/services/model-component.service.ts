import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ModelComponent} from './beans/model_component';

@Injectable({
  providedIn: 'root'
})
export class ModelComponentService {
  private serverUrl = environment.apiEndpoint;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private http: HttpClient) {
  }

  getModelComponents(userToken?: string): Observable<ModelComponent[]> {
    return this.http.get<ModelComponent[]>(`${this.serverUrl}api/model-components/?format=json`, this.httpOptions)
      .pipe(map(modelComponents => modelComponents.map(modelComponentJson => ModelComponent.parse(modelComponentJson))));
  }

  getModelComponent(id: string): Observable<ModelComponent> {
    return this.http.get<ModelComponent>(`${this.serverUrl}api/model-components/${id}/?format=json`)
      .pipe(map(modelComponent => ModelComponent.parse(modelComponent)));
  }
}
