import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Query} from './beans/query';
import {Observable} from 'rxjs';
import { Commission } from './beans/commission';

@Injectable({
  providedIn: 'root'
})
export class CommissionService {
  private serverUrl = environment.apiEndpoint;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private http: HttpClient) {
  }

  getCommissions(userToken: string): Observable<Commission> {
    let queryUrl = `${this.serverUrl}api/commissions/?format=json`;
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.get<Commission>(`${queryUrl}`, this.httpOptions)
  }

  updateCommissions(payload: any, userToken: string): Observable<Commission> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.post<Commission>(`${this.serverUrl}api/commissions/`, payload, this.httpOptions)
  }
}
