import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Query} from './beans/query';
import {Observable} from 'rxjs';
import {Page} from './beans/page';
import {map} from 'rxjs/operators';
import {ComponentFamily} from './beans/component_family';

@Injectable({
  providedIn: 'root'
})
export class ComponentFamilyService {
  private serverUrl = environment.apiEndpoint;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private http: HttpClient) {
  }

  getComponentsFamilies(query: Query, userToken: string): Observable<Page<ComponentFamily>> {
    let queryUrl = `${this.serverUrl}api/components-families/?format=json`;
    if (query) {
      for (const [key, value] of Object.entries(query)) {
        if (value) {
          queryUrl += `&${key}=${value}`;
        }
      }
    }
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.get<Page<ComponentFamily>>(queryUrl, this.httpOptions)
      .pipe(map(page => {
        page.results = page.results.map(componentFamilyJson => ComponentFamily.parse(componentFamilyJson));
        return page;
      }));
  }

  getComponentFamily(id: number, userToken: string): Observable<ComponentFamily> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.get<ComponentFamily>(`${this.serverUrl}api/components-families/${id}/?format=json`, this.httpOptions)
      .pipe(map(componentFamilyJson => ComponentFamily.parse(componentFamilyJson)));
  }
}
