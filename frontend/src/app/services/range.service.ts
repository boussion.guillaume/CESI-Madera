import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Range} from './beans/range';
import { Page } from './beans/page';

@Injectable({
  providedIn: 'root'
})
export class RangeService {
  private serverUrl = environment.apiEndpoint;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private http: HttpClient) {
  }

  getRanges(userToken: string): Observable<Page<Range>> {
    return this.http.get<Page<Range>>(`${this.serverUrl}api/ranges/?format=json`, this.httpOptions)
      .pipe(
        map(page => {
          page.results = page.results.map(rangeJson => Range.parse(rangeJson))
          return page
        })
      );
  }

  getRange(id: number, userToken: string): Observable<Range> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.get<Range>(`${this.serverUrl}api/ranges/${id}/?format=json`)
      .pipe(map(range => Range.parse(range)));
  }

  updateRange(range: Range, userToken: string): Observable<Range> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.put<Range>(`${this.serverUrl}api/ranges/${range.id}/`, range, this.httpOptions);
  }

  addRange(range: Range, userToken: string): Observable<Range> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.post<Range>(`${this.serverUrl}api/ranges/`, range, this.httpOptions);
  }

  deleteRange(id: number, userToken: string): Observable<Range> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.delete<Range>(`${this.serverUrl}api/ranges/${id}/`, this.httpOptions);
  }
}
