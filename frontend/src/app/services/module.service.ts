import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Module} from './beans/module';
import {Query} from './beans/query';

@Injectable({
  providedIn: 'root'
})
export class ModuleService {
  private serverUrl = environment.apiEndpoint;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private http: HttpClient) {
  }

  getModules(query: Query, userToken?: string): Observable<Module[]> {
    let queryUrl = `${this.serverUrl}api/modules/?format=json`;
    if (query) {
      for (const [key, value] of Object.entries(query)) {
        if (value) {
          queryUrl += `&${key}=${value}`;
        }
      }
    }
    return this.http.get<Module[]>(queryUrl, this.httpOptions)
      .pipe(map(modules => modules.map(moduleJson => Module.parse(moduleJson))));
  }

  getModule(id: string): Observable<Module> {
    return this.http.get<Module>(`${this.serverUrl}api/modules/${id}/?format=json`)
      .pipe(map(module => Module.parse(module)));
  }
}
