import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Specification} from './beans/specification';
import {map} from 'rxjs/operators';
import {UnitOfUse} from './beans/unit-of-use';

@Injectable({
  providedIn: 'root'
})
export class UnitOfUseService {
  private serverUrl = environment.apiEndpoint;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private http: HttpClient) {
  }

  getUnitsOfUse(userToken?: string): Observable<UnitOfUse[]> {
    return this.http.get<UnitOfUse[]>(`${this.serverUrl}api/units-of-use/?format=json`, this.httpOptions)
      .pipe(map(unitsOfUse => unitsOfUse.map(unitOfUseJson => UnitOfUse.parse(unitOfUseJson))));
  }

  getUnitOfUse(id: string): Observable<UnitOfUse> {
    return this.http.get<UnitOfUse>(`${this.serverUrl}api/units-of-use/${id}/?format=json`)
      .pipe(map(unitOfUse => UnitOfUse.parse(unitOfUse)));
  }
}
