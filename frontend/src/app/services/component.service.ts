import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Page} from './beans/page';
import {ComponentObject} from './beans/component';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Query} from './beans/query';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ComponentService {
  private serverUrl = environment.apiEndpoint;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private http: HttpClient) {
  }

  getComponents(query: Query, userToken: string): Observable<Page<ComponentObject>> {
    let queryUrl = `${this.serverUrl}api/components/?format=json`;
    if (query) {
      for (const [key, value] of Object.entries(query)) {
        if (value) {
          queryUrl += `&${key}=${value}`;
        }
      }
    }
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.get<Page<ComponentObject>>(queryUrl, this.httpOptions)
      .pipe(map(page => {
        page.results = page.results.map(componentJson => ComponentObject.parse(componentJson));
        return page;
      }));
  }

  getComponent(id: number, userToken: string): Observable<ComponentObject> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.get<ComponentObject>(`${this.serverUrl}api/components/${id}/?format=json`, this.httpOptions)
      .pipe(map(componentJson => ComponentObject.parse(componentJson)));
  }

  addComponent(component: any, userToken: string): Observable<ComponentObject> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.post<ComponentObject>(`${this.serverUrl}api/components/`, component, this.httpOptions)
  }

  updateComponent(payload: any, userToken: string): Observable<ComponentObject> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.put<ComponentObject>(`${this.serverUrl}api/components/${payload.id}/`, payload, this.httpOptions)
  }

  deleteComponent(id: number, userToken: string): Observable<ComponentObject> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.delete<ComponentObject>(`${this.serverUrl}api/components/${id}/`, this.httpOptions)
  }
}
