import {Injectable} from '@angular/core';
import {BehaviorSubject, fromEvent, Observable} from 'rxjs';
import {User} from './beans/user';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {filter, finalize, tap} from 'rxjs/operators';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private serverUrl = environment.apiEndpoint;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  private hasPrivilegesSubject = new BehaviorSubject(false);

  constructor(private http: HttpClient) {
    fromEvent<StorageEvent>(window, 'storage')
      .pipe(filter((event: StorageEvent) => event.key === 'madera.user'))
      .pipe(tap((event: StorageEvent) => {
        if (event.newValue) {
          this.hasPrivilegesSubject.next(this.isLoggedIn(JSON.parse(event.newValue)));
        } else {
          this.hasPrivilegesSubject.next(false);
        }
      })).subscribe((test) => {
        console.log(test);
    });

    const user = localStorage.getItem('madera.user');
    if (user) {
      this.hasPrivilegesSubject.next(this.isLoggedIn(JSON.parse(user)));
    }
  }

  logIn(username: string, password: string): Observable<User> {
    return this.http.post<User>(`${this.serverUrl}api/login/`, {username, password})
      .pipe(tap(user => {
          localStorage.setItem('madera.user', JSON.stringify(user));
          this.hasPrivilegesSubject.next(this.isLoggedIn(user));
        })
      );
  }

  logOut(userToken: string): Observable<any> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.post(`${this.serverUrl}api/logout/`, {}, this.httpOptions)
      .pipe(finalize(() => {
          localStorage.removeItem('madera.user');
          this.hasPrivilegesSubject.next(false);

        })
      );
  }

  private isLoggedIn(user: any): boolean {
    return user !== null;
  }

  // private checkPrivileges(user: any): boolean {
  //   return user.is_superuser && user.is_active;
  // }

  hasPrivileges(): Observable<boolean> {
    return this.hasPrivilegesSubject.asObservable();
  }
}
