import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Query} from './beans/query';
import {Observable} from 'rxjs';
import {Page} from './beans/page';
import {Model} from './beans/model';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ModelService {
  private serverUrl = environment.apiEndpoint;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private http: HttpClient) {
  }

  getModels(query: Query, userToken: string): Observable<Page<Model>> {
    let queryUrl = `${this.serverUrl}api/models/?format=json`;
    if (query) {
      for (const [key, value] of Object.entries(query)) {
        if (value) {
          queryUrl += `&${key}=${value}`;
        }
      }
    }
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.get<Page<Model>>(queryUrl, this.httpOptions)
      .pipe(map(page => {
        page.results = page.results.map(modelJson => Model.parse(modelJson));
        return page;
      }));
  }

  getModel(id: number, userToken: string): Observable<Model> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.get<Model>(`${this.serverUrl}api/models/${id}/?format=json`, this.httpOptions)
      .pipe(map(modelJson => Model.parse(modelJson)));
  }

  getModelsRelatedToRange(rangeId: number, userToken: string): Observable<any> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.get<any>(`${this.serverUrl}api/models/range/?range=${rangeId}`, this.httpOptions);
  }

  updateModel(model: Model, userToken: string): Observable<Model> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.put<Model>(`${this.serverUrl}api/models/${model.id}/`, Model.toJSON(model), this.httpOptions);
  }

  addModel(model: Model, userToken: string): Observable<Model> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.post<Model>(`${this.serverUrl}api/models/`, Model.toJSON(model), this.httpOptions);
  }

  deleteModel(id: number, userToken: string): Observable<Model> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.delete<Model>(`${this.serverUrl}api/models/${id}/`, this.httpOptions);
  }
}
