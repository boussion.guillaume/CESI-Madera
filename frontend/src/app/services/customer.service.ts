import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Query} from './beans/query';
import {Observable} from 'rxjs';
import {Page} from './beans/page';
import {map} from 'rxjs/operators';
import {Customer} from './beans/customer';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  private serverUrl = environment.apiEndpoint;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private http: HttpClient) {
  }

  getCustomers(query: Query, userToken: string): Observable<Page<Customer>> {
    let queryUrl = `${this.serverUrl}api/customers/?format=json`;
    if (query) {
      for (const [key, value] of Object.entries(query)) {
        if (value) {
          queryUrl += `&${key}=${value}`;
        }
      }
    }
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.get<Page<Customer>>(queryUrl, this.httpOptions)
      .pipe(map(page => {
        page.results = page.results.map(componentJson => Customer.parse(componentJson));
        return page;
      }));
  }

  getCustomer(id: number, userToken: string): Observable<Customer> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.get<Customer>(`${this.serverUrl}api/customers/${id}/?format=json`, this.httpOptions)
      .pipe(map(componentJson => Customer.parse(componentJson)));
  }

  addCustomer(payload: any, userToken: string): Observable<Customer> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.post<Customer>(`${this.serverUrl}api/customers/`, payload, this.httpOptions);
  }

  updateCustomer(payload: any, userToken: string): Observable<Customer> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.put<Customer>(`${this.serverUrl}api/customers/${payload.id}/`, payload, this.httpOptions);
  }

  deleteCustomer(id: number, userToken: string): Observable<Customer> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.delete<Customer>(`${this.serverUrl}api/customers/${id}/`, this.httpOptions);
  }
}
