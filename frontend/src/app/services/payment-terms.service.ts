import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Query} from './beans/query';
import {Observable} from 'rxjs';
import { PaymentTerms } from './beans/payment-terms';
import {map} from 'rxjs/operators';
import {UnitOfUse} from './beans/unit-of-use';

@Injectable({
  providedIn: 'root'
})
export class PaymentTermsSerivce {
  private serverUrl = environment.apiEndpoint;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private http: HttpClient) {
  }

  getPaymentTerms(userToken: string): Observable<PaymentTerms> {
    const queryUrl = `${this.serverUrl}api/payment-terms/?format=json`;
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.get<PaymentTerms>(`${queryUrl}`, this.httpOptions)
      .pipe(map(paymentTerms => PaymentTerms.parse(paymentTerms)));
  }

  updatePaymentTerms(payload: PaymentTerms, userToken: string): Observable<PaymentTerms> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.post<PaymentTerms>(`${this.serverUrl}api/payment-terms/`, payload, this.httpOptions);
  }
}
