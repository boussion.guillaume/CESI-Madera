import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Specification} from './beans/specification';

@Injectable({
  providedIn: 'root'
})
export class SpecificationService {
  private serverUrl = environment.apiEndpoint;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private http: HttpClient) {
  }

  getSpecifications(userToken?: string): Observable<Specification[]> {
    return this.http.get<Specification[]>(`${this.serverUrl}api/specifications/?format=json`, this.httpOptions)
      .pipe(map(specifications => specifications.map(specificationJson => Specification.parse(specificationJson))));
  }

  getSpecification(id: string): Observable<Specification> {
    return this.http.get<Specification>(`${this.serverUrl}api/specifications/${id}/?format=json`)
      .pipe(map(specification => Specification.parse(specification)));
  }
}
