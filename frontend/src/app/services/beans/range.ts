export class Range {
  constructor(
    public id: string,
    public name: string,
  ) {
  }

  static parse(json: any): Range {
    return new Range(
      json.id,
      json.name,
    );
  }
}
