export class Provider {
  constructor(
    public id: string,
    public name: string,
    public contact: string,
    public email: string,
    public website: string,
    public phone: string,
    public address: string,
    public fax?: string,
  ) {
  }

  static parse(json: any): Provider {
    return new Provider(
      json.id,
      json.name,
      json.contact,
      json.email,
      json.website,
      json.phone,
      json.address,
      json.fax,
    );
  }

  public toJSONFiltered(): any{
    let obj = this;
    // Dégage les valeurs null ou undefined pour le backend
    for(const[key, value] of Object.entries(obj)){
      if(!value || value === ""){
        delete obj[key];
      }
    }
    return obj;
  }
}
