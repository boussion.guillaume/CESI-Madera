export class Specification {
  constructor(
    public id: string,
    public name: string,
    public unit: number
  ) {
  }

  static parse(json: any): Specification {
    return new Specification(
      json.id,
      json.name,
      json.unit
    );
  }
}
