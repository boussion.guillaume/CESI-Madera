import {ComponentObject} from './component';

export class ModelComponent {
  constructor(
    public id: string,
    public proportion: number,
    public model: number,
    public component: ComponentObject
  ) {
  }

  static parse(json: any): ModelComponent {
    return new ModelComponent(
      json.id,
      json.proportion,
      json.model,
      ComponentObject.parse(json.component)
    );
  }
}
