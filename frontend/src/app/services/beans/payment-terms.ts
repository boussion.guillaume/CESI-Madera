export class PaymentTerms {
  constructor(
    public signatureTerm: number,
    public constructionAgreement: number,
    public constructionOpening: number,
    public fondationsCompletion: number,
    public wallsCompletion: number,
    public roofCompletion: number,
    public equipmentCompletion: number,
    public keysHandover: number) {
  }

  static parse(json: any): PaymentTerms {
    return new PaymentTerms(
      json.signatureTerm,
      json.constructionAgreement,
      json.constructionOpening,
      json.fondationsCompletion,
      json.wallsCompletion,
      json.roofCompletion,
      json.equipmentCompletion,
      json.keysHandover,
    );
  }
}
