export class User {
  constructor(
    public token: string,
    public id: number,
    public username: string,
    public first_name?: string,
    public last_name?: string,
    public email?: string,
    public is_staff?: boolean,
    public is_superuser?: boolean,
    public is_active?: boolean,
  ) {
  }

  static parse(json: any): User {
    return new User(
      json.token,
      json.id,
      json.username,
      json.first_name,
      json.last_name,
      json.email,
      json.is_staff,
      json.is_superuser,
      json.is_active
    );
  }

  static getUser(): User {
    const userData = localStorage.getItem('madera.user');
    if (userData) {
      return User.parse(JSON.parse(userData));
    }
    return null;
  }

  static isAdmin(): boolean {
    const user = User.getUser();
    if (user === null) {
      return false;
    }
    return user.is_superuser;
  }

  static isActive(): boolean {
    const user = User.getUser();
    if (user === null) {
      return false;
    }
    return user.is_active;
  }

  static isStaff(): boolean {
    const user = User.getUser();
    if (user === null) {
      return false;
    }
    return user.is_staff;
  }
}
