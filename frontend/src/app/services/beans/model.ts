export class Model {
  constructor(
    public id: number,
    public name: string,
    public specification: number,
    public unitOfUse: number,
    public range: number,
  ) {
  }

  static parse(json: any): Model {
    return new Model(
      json.id,
      json.name,
      json.specification,
      json.unit_of_use,
      json.range,
    );
  }

  static toJSON(obj: Model): any {
    return {
      id: obj.id ? obj.id : null,
      name: obj.name,
      specification: obj.specification,
      unit_of_use: obj.unitOfUse,
      range: obj.range
    };
  }
}
