import {environment} from '../../../environments/environment';
import {Customer} from './customer';
import {Commercial} from './commercial';

export class Quote {
  constructor(
    public id: string,
    public name: string,
    public amount: number,
    public date: Date,
    public file: string,
    public status: string,
    public customer: Customer,
    public user: Commercial,
  ) {
  }

  static parse(json: any): Quote {
    return new Quote(
      json.id,
      json.name,
      json.amount,
      new Date(json.date),
      json.file,
      Quote.castStatus(json.status),
      Customer.parse(json.customer),
      Commercial.parse(json.user)
    );
  }

  static get_file_url(quote: Quote): string {
    return environment.apiEndpoint.slice(0, -1) + quote.file;
  }

  static castStatus(statusValue: string): string {
    switch (statusValue) {
      case 'in_progress':
        return 'En cours';

      case 'validated':
        return 'Validé';

      case 'refused':
        return 'Refusé';
    }
  }
}
