export class Commercial {

    constructor(
      public id: string,
      public firstName: string,
      public lastName: string,
    ) {
    }
  
    static parse(json: any): Commercial {
      return new Commercial(
        json.id,
        json.first_name,
        json.last_name,
      );
    }
  }