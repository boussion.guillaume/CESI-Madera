import { ReactiveFormsModule } from '@angular/forms';

export class ComponentObject {

  constructor(
    public id: string,
    public name: string,
    public amount: number,
    public stock: number,
    public componentFamily: number,
    public provider: number,
    public specification: number,
    public unitOfUse: number
  ) {
  }

  static parse(json: any): ComponentObject {
    return new ComponentObject(
      json.id,
      json.name,
      json.amount,
      json.stock,
      json.component_family,
      json.provider,
      json.specification,
      json.unit_of_use,
    );
  }

  public toJSON(): any{
    return {
      id: this.id,
      name: this.name,
      amount: this.amount,
      stock: this.stock,
      component_family: this.componentFamily,
      provider: this.provider,
      specification: this.specification,
      unit_of_use: this.unitOfUse
    }
  }
}
