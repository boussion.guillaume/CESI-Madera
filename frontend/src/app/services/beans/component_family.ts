export class ComponentFamily {

  constructor(
    public id: string,
    public name: string,
  ) {
  }

  static parse(json: any): ComponentFamily {
    return new ComponentFamily(
      json.id,
      json.name,
    );
  }
}
