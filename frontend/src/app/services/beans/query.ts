export class Query {
  public id: number;
  public status: string;
  public page: number;
  public page_size: number;
  public search: string;
  public first_name: string;
  public last_name: string;
  public email: string;
  public ordering: string;
  public component_family: number;
  public range: number;
  public quote: number;
  public activated: boolean;
}
