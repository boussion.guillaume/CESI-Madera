export class Customer {
  constructor(
    public id: number,
    public first_name: string,
    public last_name: string,
    public email: string,
    public phone: string,
    public address: string,
    public fullname?: string
  ) {
  }

  static parse(json: any): Customer {
    return new Customer(
      json.id,
      json.first_name,
      json.last_name.toUpperCase(),
      json.email,
      json.phone,
      json.address,
      json.first_name + ' ' + json.last_name,
    );
  }

  public toJSONFiltered(): any{
  let obj = this;
  // Dégage les valeurs null ou undefined pour le backend
  for(const[key, value] of Object.entries(obj)){
    if(!value || value === ""){
      delete obj[key];
    }
  }
  return obj;
  }
}
