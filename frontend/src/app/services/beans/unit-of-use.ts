export class UnitOfUse {
  constructor(
    public id: string,
    public name: string,
    public unit: number
  ) {
  }

  static parse(json: any): UnitOfUse {
    return new UnitOfUse(
      json.id,
      json.name,
      json.unit
    );
  }
}
