export class Module {
  constructor(
    public id: string,
    public name: string,
    public specification: number,
    public unitOfUse: number,
    public range: number,
    public quote: number
  ) {
  }

  static parse(json: any): Module {
    return new Module(
      json.id,
      json.name,
      json.specification,
      json.unite_of_use,
      json.range,
      json.quote
    );
  }
}
