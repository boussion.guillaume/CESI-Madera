export class Commission{
    constructor( public enterprise: number,public commercial: number) {
      }
    
      static parse(json: any): Commission {
        return new Commission(
          json.enterpriseCommission,
          json.commercialCommission,
        );
      }
      
      static toJSON(obj: Commission) : any {
        return {
          enterpriseCommission : obj.enterprise,
          commercialCommission: obj.commercial
        }
      }
}