import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ModelComponent} from './beans/model_component';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ModuleComponentService {
  private serverUrl = environment.apiEndpoint;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private http: HttpClient) {
  }

  getModuleComponents(userToken?: string): Observable<ModelComponent[]> {
    return this.http.get<ModelComponent[]>(`${this.serverUrl}api/module-components/?format=json`, this.httpOptions)
      .pipe(map(moduleComponent => moduleComponent.map(moduleComponentJson => ModelComponent.parse(moduleComponentJson))));
  }

  getModuleComponent(id: string): Observable<ModelComponent> {
    return this.http.get<ModelComponent>(`${this.serverUrl}api/module-components/${id}/?format=json`)
      .pipe(map(moduleComponent => ModelComponent.parse(moduleComponent)));
  }
}
