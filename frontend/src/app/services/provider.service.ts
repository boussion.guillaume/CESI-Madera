import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Query} from './beans/query';
import {Observable} from 'rxjs';
import {Page} from './beans/page';
import {Provider} from './beans/provider';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProviderService {
  private serverUrl = environment.apiEndpoint;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private http: HttpClient) {
  }

  getProviders(query: Query, userToken: string): Observable<Page<Provider>> {
    let queryUrl = `${this.serverUrl}api/providers/?format=json`;
    if (query) {
      for (const [key, value] of Object.entries(query)) {
        if (value) {
          queryUrl += `&${key}=${value}`;
        }
      }
    }
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.get<Page<Provider>>(queryUrl, this.httpOptions)
      .pipe(map(page => {
        page.results = page.results.map(providerJson => Provider.parse(providerJson));
        return page;
      }));
  }

  getProvider(id: number, userToken: string): Observable<Provider> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.get<Provider>(`${this.serverUrl}api/providers/${id}/?format=json`, this.httpOptions)
      .pipe(map(providerJson => Provider.parse(providerJson)));
  }

  
  addProvider(provider: Provider, userToken: string): Observable<Provider> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.post<Provider>(`${this.serverUrl}api/providers/`, provider.toJSONFiltered(), this.httpOptions);
  }  

  updateProvider(provider: Provider, userToken: string): Observable<Provider> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.patch<Provider>(`${this.serverUrl}api/providers/${provider.id}/`, provider, this.httpOptions);
  }

  deleteProvider(id: number, userToken: string): Observable<Provider> {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${userToken}`);
    return this.http.delete<Provider>(`${this.serverUrl}api/providers/${id}/`, this.httpOptions);
  }

}
