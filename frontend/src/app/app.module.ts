import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './components/login/login.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ComponentsListComponent} from './components/component/components-list/components-list.component';
import {AppInitService} from './services/app-init.service';
import {SidenavComponent} from './components/sidenav/sidenav.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {AuthService} from './services/auth.service';
import {ComponentModalComponent} from './components/component/component-modal/component-modal.component';
import {ComponentDetailComponent} from './components/component/component-detail/component-detail.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {RoutingState} from './services/routing.state';
import {ComponentFamilyListComponent} from './components/component-family/component-family-list/component-family-list.component';
import {ComponentFamilyDetailComponent} from './components/component-family/component-family-detail/component-family-detail.component';
import {ComponentFamilyModalComponent} from './components/component-family/component-family-modal/component-family-modal.component';
import {ProvidersListComponent} from './components/provider/providers-list/providers-list.component';
import {ProviderModalComponent} from './components/provider/provider-modal/provider-modal.component';
import {ProviderDetailComponent} from './components/provider/provider-detail/provider-detail.component';
import {SharedService} from './services/shared.service';
import {CustomerListComponent} from './components/customer/customer-list/customer-list.component';
import {CustomerDetailComponent} from './components/customer/customer-detail/customer-detail.component';
import {CustomerModalComponent} from './components/customer/customer-modal/customer-modal.component';
import {QuoteFormComponent} from './components/quote/quote-form/quote-form.component';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { ModelListComponent } from './components/model/model-list/model-list.component';
import { ModelDetailComponent } from './components/model/model-detail/model-detail.component';
import { ModelModalComponent } from './components/model/model-modal/model-modal.component';
import { RangeListComponent } from './components/range/range-list/range-list.component';
import { RangeDetailComponent } from './components/range/range-detail/range-detail.component';
import { RangeDeleteConfirmationComponent } from './components/range/range-delete-confirmation/range-delete-confirmation.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { QuoteListComponent } from './components/quote/quote-list/quote-list.component';
import { RangeModalComponent } from './components/range/range-modal/range-modal.component';
import { ToastrModule } from 'ngx-toastr';
import { QuoteDetailComponent } from './components/quote/quote-detail/quote-detail.component';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import {ChartsModule} from 'ng2-charts';
import {CommissionComponent} from './components/commission/commission.component'
import {PaymentTermsComponent} from './components/payment-terms/payment-terms.component'


export function init_app(appLoadService: AppInitService) {
  return () => appLoadService.init();
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ComponentsListComponent,
    SidenavComponent,
    DashboardComponent,
    ComponentModalComponent,
    ComponentDetailComponent,
    ComponentFamilyListComponent,
    ComponentFamilyDetailComponent,
    ComponentFamilyModalComponent,
    ProvidersListComponent,
    ProviderModalComponent,
    ProviderDetailComponent,
    CustomerListComponent,
    CustomerDetailComponent,
    CustomerModalComponent,
    QuoteFormComponent,
    ModelListComponent,
    ModelDetailComponent,
    ModelModalComponent,
    RangeListComponent,
    RangeDetailComponent,
    RangeModalComponent,
    RangeDeleteConfirmationComponent,
    PageNotFoundComponent,
    QuoteListComponent,
    QuoteDetailComponent,
    CommissionComponent,
    PaymentTermsComponent
  ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        NgbModule,
        FormsModule,
        AppRoutingModule,
        FontAwesomeModule,
        ReactiveFormsModule,
        AutocompleteLibModule,
        ToastrModule.forRoot(),
        PdfViewerModule,
        ChartsModule,
    ],
  providers: [
    RoutingState,
    AppInitService,
    AuthService,
    SharedService,
    {
      provide: APP_INITIALIZER,
      useFactory: init_app,
      deps: [AppInitService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
