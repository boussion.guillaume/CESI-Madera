import {Component, OnInit} from '@angular/core';
import {AuthService} from './services/auth.service';
import {Router} from '@angular/router';
import {RoutingState} from './services/routing.state';
import {User} from './services/beans/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'frontend';
  public isLogged: boolean;
  public user: User;
  public navbarOpen = true;
  public textButtonMenu = 'Réduire le menu';


  constructor(private as: AuthService,
              private router: Router,
              private routingState: RoutingState) {
    this.routingState.loadRouting();
    this.as.hasPrivileges().subscribe((isLogged) => {
      this.isLogged = isLogged;
      if (!isLogged && !this.router.url.startsWith('/login')) {
        this.router.navigate(['/login']);
      }
    });
  }

  ngOnInit(): void {
    this.user = User.getUser();
  }

  toggleNavbar(): void {
    this.navbarOpen = !this.navbarOpen;
    if (this.navbarOpen) {
      this.textButtonMenu = 'Réduire le menu';
    } else {
      this.textButtonMenu = 'Etendre le menu';
    }
  }

  logOut(): void {
    this.user = User.getUser();
    this.as.logOut(this.user.token).subscribe((value) => {
      // this.router.navigate(['/login']);
      console.log(value);
    }, (error) => {
      console.error(error);
    });
  }
}
