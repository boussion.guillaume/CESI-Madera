declare var window: any;

export class DynamicEnvironment {
  public get apiEndpoint(): string {
    return window.config.backend;
  }

  public get websocketEndpoint(): string {
    return window.config.backendWebsocket;
  }
}
